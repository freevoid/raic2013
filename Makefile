PYTHON=python2

SERVER_STARTUP_SLEEP=0.3
TEST_MANY_COUNT=5
SRCDIR=./src
STRATEGY_RUNNER=$(SRCDIR)/Runner.py
LOCAL_RUNNER_DIR=./local_runner
RESULT_FILE=./result.txt
SRCFILES=$(wildcard $(SRCDIR)/*)
SRCPYFILES=$(wildcard $(SRCDIR)/*.py)
TEST_RUN_CHECKER=$(PYTHON) ./util/check_test_run_results.py
TEST_HISTORY_PATH=./game_history_test.pickle
HISTORY_PATH=./dump.pickle
SEEDS_FILE=./bad_seeds_test.txt
SEED=
PORT=31001
MAP=default
GAME_CONF=4x3

package.zip: $(SRCFILES) test
	rm $@
	cd $(SRCDIR) && zip ../package.zip *.py

package:
	rm package.zip
	cd $(SRCDIR) && zip ../package.zip *.py


test: repeat_prepared
	@$(TEST_RUN_CHECKER) $(RESULT_FILE)


repeat_prepared:
	$(PYTHON) $(STRATEGY_RUNNER) -r -g $(TEST_HISTORY_PATH)


testrun: repeat_prepared runserver run test


Run: runserver run


saverun: runserver
	sleep $(SERVER_STARTUP_SLEEP)
	$(PYTHON) $(STRATEGY_RUNNER) --seed "$(SEED)" --port "$(PORT)" -g "$(HISTORY_PATH)"
	cat $(RESULT_FILE)


run:
	sleep $(SERVER_STARTUP_SLEEP)
	$(PYTHON) $(STRATEGY_RUNNER) --seed "$(SEED)" --port "$(PORT)"
	cat $(RESULT_FILE)


rungui: runserver_gui run


runserver_gui:
	$(PYTHON) ./util/local_runner.py "$(LOCAL_RUNNER_DIR)/local-runner-console.properties" --seed "$(SEED)" --port "$(PORT)" --map "$(MAP)" --conf "$(GAME_CONF)" -g &


runserver:
	$(PYTHON) ./util/local_runner.py "$(LOCAL_RUNNER_DIR)/local-runner-console.properties" --seed "$(SEED)" --port "$(PORT)" --map "$(MAP)" --conf "$(GAME_CONF)" &


deploy:
	#scp Makefile desh@do.desh.su:~/workspace/raic2013/
	#scp src/*.py desh@do.desh.su:~/workspace/raic2013/src/

testmany:
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) --result_file=$(RESULT_FILE) --port=$(PORT) --map=$(MAP) --conf=$(GAME_CONF)

testmany_allmaps:
	rm -vf run_*.log
	rm -vf run_*.seeds.txt
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) -l run_default.log --map default --conf=$(GAME_CONF) --port 33001 --result_file=result_default.txt &
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) -l run_cheeser.log --map cheeser --conf=$(GAME_CONF) --port 34001 --result_file=result_cheeser.txt &
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) -l run_map01.log --map map01 --conf=$(GAME_CONF) --port 35001 --result_file=result_map01.txt &
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) -l run_map02.log --map map02 --conf=$(GAME_CONF) --port 36001 --result_file=result_map02.txt &
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) -l run_map03.log --map map03 --conf=$(GAME_CONF) --port 37001 --result_file=result_map03.txt &

collect_bad_seeds:
	cat run_*.seeds.txt >> bad_seeds_test.txt

runbadseeds:
	$(PYTHON) ./util/get_win_stats_with_bots.py -n $(TEST_MANY_COUNT) -s $(SEEDS_FILE)


.PHONY: run runserver testrun package test repeat_prepared deploy
