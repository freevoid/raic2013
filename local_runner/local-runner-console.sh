pushd `dirname $0` > /dev/null
SCRIPTDIR=`pwd`
popd > /dev/null

if command -v cygpath >/dev/null 2>&1; then
    SCRIPTDIR=`cygpath -w "$SCRIPTDIR"`
fi

SEED=$1

if [ -z "$2" ]; then
    PROPERTIES=$SCRIPTDIR/local-runner-console.properties
else
    PROPERTIES=$2
fi

java -cp ".:*:$SCRIPTDIR/*" -jar "$SCRIPTDIR/local-runner.jar" $PROPERTIES $SEED
