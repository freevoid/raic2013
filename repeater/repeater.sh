pushd `dirname $0` > /dev/null
SCRIPTDIR=`pwd`
popd > /dev/null

if command -v cygpath >/dev/null 2>&1; then
    SCRIPTDIR=`cygpath -w "$SCRIPTDIR"`
fi

java -cp ".:*:$SCRIPTDIR/*" -jar $SCRIPTDIR/repeater.jar $1
