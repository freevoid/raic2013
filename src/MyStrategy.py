import time
from model_types import ActionType, TrooperType, Trooper
from game_master import GameMaster
from util import get_trooper_repr, get_action_repr

class MyStrategy:

    master = None

    def __init__(self, debug=False, verbose=False):
        self.debug = debug
        self.verbose = verbose

    def move(self, me, world, game, move):
        started = time.time()
        #if me.action_points < game.standing_move_cost and me.type != TrooperType.FIELD_MEDIC:
        #    return

        if MyStrategy.master is None:
            MyStrategy.master = GameMaster(me.player_id, world, game, debug=self.debug, verbose=self.verbose)

        movement = MyStrategy.master.get_move(me, world)
        if movement is not None:
            (action_type, direction) = movement
        else:
            action_type, direction = ActionType.END_TURN, None
        move.action = action_type
        if isinstance(direction, tuple):
            x, y = direction
            move.x = x
            move.y = y
        else:
            move.direction = direction

        elapsed = time.time() - started

        print "[%d]\t%2d\t%23s\t%15s\t%9s\t%.5f" % (
            world.move_index,
            me.action_points,
            get_trooper_repr(me),
            get_action_repr(move.action),
            direction,
            elapsed)
