from __future__ import division

import random
import time
from MyStrategy import MyStrategy
from RemoteProcessClient import RemoteProcessClient, GameDumpClient
from model.Move import Move
import cPickle as pickle

import os
import urllib2

URL_BASE = 'http://russianaicup.ru/boombox/data/dumps/'


def download_game_dump(token):
    dump_path = os.path.join('game_dumps', token)
    if not os.path.isfile(dump_path):
        print "Downloading game dump.."
        game_dump_url = URL_BASE + token
        with open(dump_path, 'wb') as fp:
            fp.write(urllib2.urlopen(game_dump_url).read())
        print "Saved to %s" % dump_path
    return dump_path


class Runner:
    def __init__(self, debug=False, verbose=False, history_path=None, dump_path=None,
            move_index=None, port=31001, seed=None):
        self.port = port
        self.dump_path = dump_path
        self.history_path = history_path
        self.debug = debug
        self.verbose = verbose
        #if sys.argv.__len__() == 4:
        #    self.remote_process_client = RemoteProcessClient(sys.argv[1], int(sys.argv[2]))
        #    self.token = sys.argv[3]
        #else:
        if not self.history_path:
            self.history_path = None# 'game_history.pickle'

        self.move_index = move_index

        self.token = "0000000000000000"
        self.seed = seed

    def repeat_dump(self):
        self.remote_process_client = GameDumpClient(self.dump_path)
        return self._run(self.move_index)

    def repeat_pickled(self):
        game_state = load_game_history(self.history_path)
        game_history = game_state['history']
        team_size = game_state['team_size']
        game = game_state['game']

        seed = game_state.get('seed')
        if seed is not None:
            random.seed(seed)

        strategies = []

        for strategy_index in xrange(team_size):
            strategies.append(MyStrategy(debug=self.debug, verbose=self.verbose))

        max_elapsed = 0
        sum_elapsed = 0

        for player_context in game_history:
            if self.move_index is None or self.move_index == player_context.world.move_index:
                started = time.time()
                player_trooper = player_context.trooper
                move = Move()
                strategies[player_trooper.teammate_index].move(player_trooper, player_context.world, game, move)
                elapsed = time.time() - started
                max_elapsed = max(elapsed, max_elapsed)
                sum_elapsed += elapsed

        print "Max time for turn: %.5f sec." % max_elapsed
        print "Average time per turn: %.5f sec." % (sum_elapsed / len(game_history))

    def run(self):
        self.remote_process_client = RemoteProcessClient("127.0.0.1", self.port)
        return self._run()

    def _run(self, move_index=None):
        game_history = []
        team_size = None
        game = None
        max_elapsed = 0
        sum_elapsed = 0

        try:
            self.remote_process_client.write_token(self.token)
            team_size = self.remote_process_client.read_team_size()
            self.remote_process_client.write_protocol_version()
            game = self.remote_process_client.read_game_context()

            strategies = []

            for strategy_index in xrange(team_size):
                strategies.append(MyStrategy(debug=self.debug, verbose=self.verbose))

            while True:
                started = time.time()
                player_context = self.remote_process_client.read_player_context()
                if player_context is None:
                    break

                game_history.append(player_context)

                player_trooper = player_context.trooper

                if move_index is None or move_index == player_context.world.move_index:
                    move = Move()
                    strategies[player_trooper.teammate_index].move(player_trooper, player_context.world, game, move)
                    self.remote_process_client.write_move(move)
                    elapsed = time.time() - started
                    max_elapsed = max(elapsed, max_elapsed)
                    sum_elapsed += elapsed
        finally:
            self.remote_process_client.close()
            if self.history_path and game_history:
                game_state = {
                    'game': game,
                    'history': game_history,
                    'team_size': team_size,
                    'seed': self.seed,
                }
                dump_game_history(game_state, self.history_path)
                print "Saved game to '%s'" % self.history_path


        if len(game_history) == 0:
            return

        print "Max time for turn: %.5f sec." % max_elapsed
        print "Average time per turn: %.5f sec." % (sum_elapsed / len(game_history))


def dump_game_history(history, path):
    with open(path, 'wb') as fp:
        fp.write(pickle.dumps(history))


def load_game_history(path):
    with open(path, 'rb') as fp:
        return pickle.loads(fp.read())


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--game', dest='history_path', default='')
    parser.add_argument('-r', '--repeat', action='store_true')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-d', '--debug', action='store_true')
    parser.add_argument('-s', '--seed', default=None)
    parser.add_argument('-p', '--port', default=31001, type=int)
    parser.add_argument('-t', '--token', default=None)
    parser.add_argument('-m', '--move', default=None, type=int)
    parser.add_argument('--dump', dest='dump_path', default='')
    args = parser.parse_args()

    if args.seed:
        random.seed(int(args.seed))

    if args.token:
        args.dump_path = download_game_dump(args.token)

    runner = Runner(debug=args.debug, verbose=args.verbose, history_path=args.history_path, dump_path=args.dump_path, move_index=args.move, port=args.port, seed=args.seed)

    if args.repeat and args.history_path:
        runner.repeat_pickled()
    elif args.dump_path:
        runner.repeat_dump()
    else:
        runner.run()
