import copy
from collections import namedtuple, deque
import time

from model_types import ActionType, TrooperStance, TrooperType, BonusType
from util import get_action_repr

Position = namedtuple('Position', 'x y')

# We will not use medikit if we use less than MEDIKIT_POTENTIAL_RATIO of it's power.
MEDIKIT_POTENTIAL_RATIO = 0.8
HEAL_POTENTIAL_RATIO = 1.0


class ActionState(object):

    def __init__(self,
            id, type, action_points, stance, shoot_cost,
            holding_medikit, holding_grenade, holding_field_ration,
            position, damages, hitpoints, shooting_ranges,
            initial_action_points, maximal_hitpoints,
            can_heal):
        self.id = id
        self.action_points = action_points
        self.stance = stance
        self.shoot_cost = shoot_cost
        self.holding_medikit = holding_medikit
        self.holding_grenade = holding_grenade
        self.holding_field_ration = holding_field_ration
        self.position = position
        self.type = type
        self.can_heal = can_heal
        self.damages = damages
        self.hitpoints = hitpoints
        self.initial_hitpoints = hitpoints
        self.initial_action_points = initial_action_points
        self.shooting_ranges = shooting_ranges
        self.maximal_hitpoints = maximal_hitpoints
        self.gained_points = 0
        self.killed = 0
        self.healed_hitpoints = 0

        self.picked_bonuses = []
        self.enemies_hitpoints = {}
        self.teammates_hitpoints = {}

    @property
    def shooting_range(self):
        return self.shooting_ranges[self.stance]

    @property
    def damage(self):
        return self.damages[self.stance]

    @classmethod
    def from_trooper(cls, trooper, knowledge):
        damages = {
            TrooperStance.STANDING: trooper.standing_damage,
            TrooperStance.KNEELING: trooper.kneeling_damage,
            TrooperStance.PRONE: trooper.prone_damage,
        }

        if trooper.type != TrooperType.SNIPER:
            shooting_ranges = {
                TrooperStance.STANDING: trooper.shooting_range,
                TrooperStance.KNEELING: trooper.shooting_range,
                TrooperStance.PRONE: trooper.shooting_range,
            }
        else:
            base_shooting_range = trooper.shooting_range

            if trooper.stance == TrooperStance.PRONE:
                base_shooting_range -= knowledge.constants.sniper_prone_shooting_range_bonus
            elif trooper.stance == TrooperStance.KNEELING:
                base_shooting_range -= knowledge.constants.sniper_kneeling_shooting_range_bonus
            else:
                base_shooting_range -= knowledge.constants.sniper_standing_shooting_range_bonus

            shooting_ranges = {
                TrooperStance.STANDING: base_shooting_range + knowledge.constants.sniper_standing_shooting_range_bonus,
                TrooperStance.KNEELING: base_shooting_range + knowledge.constants.sniper_kneeling_shooting_range_bonus,
                TrooperStance.PRONE: base_shooting_range + knowledge.constants.sniper_prone_shooting_range_bonus,
            }

        context = knowledge.get_trooper_context(trooper)

        state = cls(
            id=trooper.id,
            action_points=trooper.action_points,
            stance=trooper.stance,
            shoot_cost=trooper.shoot_cost,
            holding_medikit=trooper.holding_medikit,
            holding_grenade=trooper.holding_grenade,
            holding_field_ration=trooper.holding_field_ration,
            position=Position(trooper.x, trooper.y),
            damages=damages,
            type=trooper.type,
            hitpoints=trooper.hitpoints,
            shooting_ranges=shooting_ranges,
            initial_action_points=trooper.initial_action_points,
            maximal_hitpoints=trooper.maximal_hitpoints,
            can_heal=context.can_heal,
        )

        for enemy in knowledge.get_enemies():
            #print "ADDED ENEMY", enemy.x, enemy.y, enemy.hitpoints
            state.enemies_hitpoints[(enemy.x, enemy.y)] = enemy.hitpoints
        for teammate in knowledge.get_teammates(trooper):
            #print "ADDED TEAMMATE", teammate.x, teammate.y, teammate.hitpoints
            state.teammates_hitpoints[(teammate.x, teammate.y)] = teammate.hitpoints

        return state

    def __repr__(self):
        return '<ActionState: %s>' % self.__dict__

    def copy(self, action):
        shallow_copy = copy.copy(self)

        if action.type == ActionType.SHOOT or action.type == ActionType.THROW_GRENADE:
            shallow_copy.enemies_hitpoints = self.enemies_hitpoints.copy()

        if action.type == ActionType.HEAL or action.type == ActionType.USE_MEDIKIT:
            shallow_copy.teammates_hitpoints = self.teammates_hitpoints.copy()

        return shallow_copy

#= namedtuple('ActionState', 'action_points stance shoot_cost holding_medikit holding_grenade holding_field_ration can_heal')


def get_state_from_trooper(trooper, knowledge):
    return ActionState.from_trooper(trooper, knowledge)


def get_best_actions(trooper, knowledge, goal_func, verbose=True, timeout=1.2):
    return  _get_best_actions(
        get_state_from_trooper(trooper, knowledge),
        knowledge,
        goal_func)


def simulate_action_with_count_in_mind(action, cost, state, knowledge):
    count = action.count
    while count:
        state = simulate_action_with_context(action, cost, state, knowledge)
        count -=1

    return (action,)*action.count, state


def _get_best_actions(state, knowledge, goal_func, verbose=True, timeout=1.3):
    pool = deque()

    for action, cost in get_possible_actions_with_context(state, knowledge):
        new_actions, new_state = simulate_action_with_count_in_mind(action, cost, state, knowledge)
        pool.append((new_actions, new_state))

    best_score = goal_func(state, knowledge)
    best_actions = None
    best_state = None
    max_pool_len = 0
    total_combinations = 0

    started = time.time()

    while pool:
        if total_combinations % 500 == 0:
            elapsed = time.time() - started
            if elapsed > timeout:
                print "TIMEOUT", elapsed
                break
        total_combinations += 1
        actions, state = pool.pop()
        # Popleft explodes the pool size
        #actions, state = pool.popleft()

        score = goal_func(state, knowledge)
        if score > best_score:
            best_score = score
            best_actions = actions
            best_state = state
            #print "NEW BEST ACTIONS:", best_actions

        if len(pool) > max_pool_len:
            max_pool_len = len(pool)

        #has_potential = False
        for action, cost in get_possible_actions_with_context(state, knowledge):
            new_actions, new_state = simulate_action_with_count_in_mind(action, cost, state, knowledge)
            pool.append((actions + new_actions, new_state))

    if verbose:
        print "MAX POOL SIZE:", max_pool_len
        print "TOTAL COMBINATIONS TRAVERSED:", total_combinations
        print "BEST SCORE:", best_score
        print "SEQUENCE LENGTH:", len(best_actions) if best_actions is not None else None

    return best_actions, best_score, best_state


class Action(object):

    def __init__(self, type, position=None, count=1):
        self.type = type
        self.position = position
        self.count = count

    def __repr__(self):
        s =  '<Action: %s:%s>' % (get_action_repr(self.type), self.position)
        if self.count > 1:
            return s + '*%d' % self.count
        else:
            return s


def simulate_grenade(state, position, knowledge):
    postuple = position.x, position.y

    main_target = knowledge.get_trooper_at(postuple)

    if main_target is not None:
        enemy_hitpoints = state.enemies_hitpoints[postuple]
        # Check to not count twice already killed trooper
        if enemy_hitpoints > 0:
            damage = min(
                enemy_hitpoints, knowledge.constants.grenade_direct_damage)
            state.gained_points += damage
            state.enemies_hitpoints[postuple] -= damage
            if damage == enemy_hitpoints:
                state.killed += 1
                state.gained_points += knowledge.constants.trooper_elimination_score

    collateral_targets = knowledge.get_neighboor_troopers_iter(position)

    for target in collateral_targets:
        if target.id == state.id:
            continue

        if target.teammate:
            raise Exception("Thrown grenade to a teammate!")

        target_pos = target.x, target.y
        enemy_hitpoints = state.enemies_hitpoints.get(target_pos)

        if enemy_hitpoints == 0:
            continue

        damage = min(
            enemy_hitpoints, knowledge.constants.grenade_collateral_damage)
        state.gained_points += damage
        state.enemies_hitpoints[target_pos] -= damage

        if damage == enemy_hitpoints:
            state.killed += 1
            state.gained_points += knowledge.constants.trooper_elimination_score


def simulate_action_with_context(action, cost, state, knowledge):
    state = state.copy(action=action)
    state.action_points -= cost
    action_type = action.type

    if action_type == ActionType.THROW_GRENADE:
        state.holding_grenade = False
        simulate_grenade(state, action.position, knowledge)

    elif action_type == ActionType.EAT_FIELD_RATION:
        state.holding_field_ration = False
        state.action_points = min(
            state.action_points + knowledge.constants.field_ration_bonus_action_points,
            state.initial_action_points)

    elif action_type == ActionType.LOWER_STANCE:
        state.stance -= 1

    elif action_type == ActionType.RAISE_STANCE:
        state.stance += 1

    elif action_type == ActionType.MOVE:
        state.position = action.position
        # check if we've picked up a bonus after a move
        bonus = knowledge.get_bonus_at(state.position)

        if (bonus is not None
            and (bonus.x, bonus.y) not in state.picked_bonuses
            and not knowledge.trooper_has_bonus(state, bonus.type)):

            if bonus.type == BonusType.GRENADE:
                state.holding_grenade = True
            elif bonus.type == BonusType.MEDIKIT:
                state.holding_medikit = True
            elif bonus.type == BonusType.FIELD_RATION:
                state.holding_field_ration = True

            state.picked_bonuses = state.picked_bonuses[:]
            state.picked_bonuses.append((bonus.x, bonus.y))

    elif action_type == ActionType.SHOOT:
        enemy = knowledge.get_trooper_at(action.position)
        if enemy is not None:
            epos = (enemy.x, enemy.y)
            enemy_hitpoints = state.enemies_hitpoints[epos]
            assert enemy_hitpoints > 0, 'Generated action to shoot already killed enemy'

            damage = min(state.damage, enemy_hitpoints)
            state.enemies_hitpoints[epos] -= damage
            state.gained_points += damage

            if enemy_hitpoints == damage:
                state.killed += 1
                state.gained_points += knowledge.constants.trooper_elimination_score

    elif action_type == ActionType.USE_MEDIKIT:
        state.holding_medikit = False

        if action.position == state.position:
            heal_effect = min(
                knowledge.constants.medikit_heal_self_bonus_hitpoints,
                state.maximal_hitpoints - state.hitpoints)
            state.hitpoints += heal_effect
            state.healed_hitpoints += heal_effect
        else:
            teammate = knowledge.get_trooper_at(action.position)
            current_hitpoints = state.teammates_hitpoints[action.position]
            heal_effect = min(
                knowledge.constants.medikit_bonus_hitpoints,
                teammate.maximal_hitpoints - current_hitpoints)
            state.teammates_hitpoints[action.position] += heal_effect
            state.healed_hitpoints += heal_effect

    elif action_type == ActionType.HEAL:
        if action.position == state.position:
            heal_effect = min(
                knowledge.constants.field_medic_heal_self_bonus_hitpoints,
                state.maximal_hitpoints - state.hitpoints)
            state.hitpoints += heal_effect
            state.healed_hitpoints += heal_effect
        else:
            teammate = knowledge.get_trooper_at(action.position)
            current_hitpoints = state.teammates_hitpoints[action.position]
            heal_effect = min(
                knowledge.constants.field_medic_heal_bonus_hitpoints,
                teammate.maximal_hitpoints - current_hitpoints)
            state.teammates_hitpoints[action.position] += heal_effect
            state.healed_hitpoints += heal_effect

    return state


def get_possible_actions_with_context(state, knowledge):

    # MOVE
    # Generally, we could move whenever it is moveable (cell is free)
    # and there are no troopers at the target cell.
    cost = knowledge.get_move_cost(state.stance)
    if cost <= state.action_points:
        for direction, position in knowledge.world_map.get_moveable_directions(state.position):
            blocking_trooper = knowledge.get_trooper_at(position)
            if blocking_trooper is not None and blocking_trooper.id != state.id:
                # Check if a trooper blocking our way is the enemy we've just killed
                if state.enemies_hitpoints.get(position, None) != 0:
                    continue

            yield Action(ActionType.MOVE, position=Position(*position)), cost

    # SHOOT
    # Generally, we could shoot in any shootable enemy, but
    # we must check if we've already killed the trooper using state.enemies_hitpoints.
    if state.shoot_cost <= state.action_points:
        enemies = knowledge.get_shootable_enemies(state.position, state.stance, state.shooting_range)
        for e in enemies:
            epos = (e.x, e.y)
            # We expect to always have all visible enemies in enemies_hitpoints
            hitpoints = state.enemies_hitpoints[epos]
            if hitpoints > 0:
                yield Action(ActionType.SHOOT, position=Position(*epos)), state.shoot_cost

    # LOWER_STANCE
    # We could lower our stance only if we are not laying already.
    if state.stance != TrooperStance.PRONE:
        cost = knowledge.get_static_action_cost(ActionType.LOWER_STANCE)
        if cost <= state.action_points:
            yield Action(ActionType.LOWER_STANCE), cost

    # RAISE_STANCE
    # We could raise our stance only if we are not standing already
    if state.stance != TrooperStance.STANDING:
        cost = knowledge.get_static_action_cost(ActionType.RAISE_STANCE)
        if cost <= state.action_points:
            yield Action(ActionType.RAISE_STANCE), cost

    # THROW_GRENADE
    # Similar requirement to shooting. We also check if there are teammates
    # aroung the explosion point.
    if state.holding_grenade:
        cost = knowledge.get_static_action_cost(ActionType.THROW_GRENADE)
        if cost <= state.action_points:
            enemies = knowledge.get_enemies_within_grenade_range(state.position)
            # TODO: throw grenades at the neighbooring cell as well
            for e in enemies:
                epos = (e.x, e.y)
                # 1) Check if we've already killed the enemy
                # We expect to always have all visible enemies in enemies_hitpoints
                hitpoints = state.enemies_hitpoints[epos]
                if hitpoints > 0:
                    # 2) Check if there are a teammate near the enemy (to avoid self-blowing)
                    if not any(knowledge.get_neighboor_teammates_iter(e)):
                        yield Action(ActionType.THROW_GRENADE, position=Position(e.x, e.y)), cost

    # EAT_FIELD_RATION
    # We should generate this action ONLY if it makes sense to fulfill our action
    # points. i.e. difference between our max score (10) and our current points
    # must be greater or equal to the profit from field ration.
    if state.holding_field_ration:
        cost = knowledge.get_static_action_cost(ActionType.EAT_FIELD_RATION)
        if cost <= state.action_points:
            if ((state.initial_action_points - state.action_points) >= knowledge.constants.field_ration_bonus_action_points - cost):
                yield Action(ActionType.EAT_FIELD_RATION), cost

    # USE_MEDIKIT
    # We could use medikit to neighboors ONLY if there are neighboors
    # with big loss of hitpoints (or to ourselves, in which case we must have
    # a loss of hitpoints).
    if state.can_heal and state.holding_medikit:
        cost = knowledge.get_static_action_cost(ActionType.USE_MEDIKIT)
        if cost <= state.action_points:
            medikit_threshold_for_self = MEDIKIT_POTENTIAL_RATIO*knowledge.constants.medikit_heal_self_bonus_hitpoints
            medikit_threshold_for_mates = MEDIKIT_POTENTIAL_RATIO*knowledge.constants.medikit_bonus_hitpoints

            # Check if we could heal ourselves
            if state.maximal_hitpoints - state.hitpoints >= medikit_threshold_for_self:
                yield Action(ActionType.USE_MEDIKIT, state.position), cost

            for mate in knowledge.get_neighboor_teammates_iter(state.position):
                # We could get ourserlves because knowledge is static.
                if mate.id == state.id:
                    continue
                mate_pos = (mate.x, mate.y)
                mate_hitpoints = state.teammates_hitpoints[mate_pos]
                if mate.maximal_hitpoints - mate_hitpoints >= medikit_threshold_for_mates:
                    yield Action(ActionType.USE_MEDIKIT, Position(*mate_pos)), cost

    if state.can_heal:
        cost = knowledge.get_static_action_cost(ActionType.HEAL)
        if cost <= state.action_points:
            heal_threshold_for_self = HEAL_POTENTIAL_RATIO*knowledge.constants.field_medic_heal_self_bonus_hitpoints
            heal_threshold_for_mates = HEAL_POTENTIAL_RATIO*knowledge.constants.field_medic_heal_bonus_hitpoints

            # Check if we could heal ourselves
            healing_potential = state.maximal_hitpoints - state.hitpoints
            if healing_potential >= heal_threshold_for_self:
                count = 1
                if state.action_points >= 4*cost and healing_potential >= 2*knowledge.constants.field_medic_heal_self_bonus_hitpoints:
                    count = 2
                yield Action(ActionType.HEAL, state.position, count=count), cost

            for mate in knowledge.get_neighboor_teammates_iter(state.position):
                # We could get ourselves because knowledge is static.
                if mate.id == state.id:
                    continue
                mate_pos = (mate.x, mate.y)
                mate_hitpoints = state.teammates_hitpoints[mate_pos]
                healing_potential = mate.maximal_hitpoints - mate.hitpoints
                if healing_potential >= heal_threshold_for_mates:
                    count = 1
                    if state.action_points >= 4*cost and healing_potential >= 2*knowledge.constants.field_medic_heal_bonus_hitpoints:
                        count = 2
                    yield Action(ActionType.HEAL, Position(*mate_pos), count=count), cost
