from random import getrandbits
import random
from model_types import Direction, TrooperStance, ActionType
import actions_combinations
from util import get_action_repr
from goal_functions import attack_goal_function

MOVES_BEFORE_ASKING_FOR_DISPOSITION_INFO = 5

class BehaviorBase(object):

    def __init__(self, verbose=False, debug=False):
        self.verbose = verbose
        self.debug = debug

    def get_leader_to_follow(self, trooper, knowledge, context):
        mate = None
        mates = knowledge.get_teammates(trooper)
        if mates:
            if context.leader_type is not None:
                # Assumes that leader always set to existing trooper
                mate = filter(lambda m: m.type == context.leader_type, mates)[0]
            else:
                mates.sort(key=lambda m: m.type)
                mate = mates[0]
        return mate

    def move_in_random_direction(self, trooper, knowledge):
        if not knowledge.can_move(trooper):
            return

        available_positions = [
            position
            for direction, position in knowledge.world_map.get_moveable_directions((trooper.x, trooper.y))
            if not knowledge.has_trooper_at(position)
        ]

        if not available_positions:
            return

        index = random.randrange(len(available_positions))
        return (ActionType.MOVE, available_positions[index])

    def move_in_path_if_make_sense(self, trooper, knowledge, context, stop_nearby=False):
        step = context.step
        path = context.path

        # Check if we are not told the path
        if path is None:
            return

        # Check if we already reached the destination
        if stop_nearby and step == len(path) - 1:
            return

        if step == len(path):
            return

        pos = path[step]

        # Check if we are blocked by some other folk
        if knowledge.has_trooper_at(pos):
            return

        # If we are in the lower stance, deciding what is better:
        #  1) To stand up, move, and sit
        #  2) To move without changing stance
        #  3) To end turn (if we cannot sit at the end we should not move)
        if trooper.stance != TrooperStance.STANDING:
            points_to_stand_up = knowledge.get_points_to_change_stance(trooper.stance, TrooperStance.STANDING)

            standing_move_potential = (trooper.action_points - points_to_stand_up - knowledge.constants.stance_change_cost) // knowledge.constants.standing_move_cost

            kneeling_move_potential = trooper.action_points // knowledge.get_action_cost(trooper, ActionType.MOVE)

            if standing_move_potential <= 0:
                if kneeling_move_potential <= 0:
                    return
                else:
                    return ActionType.RAISE_STANCE, None
            elif kneeling_move_potential <= 0:
                return ActionType.RAISE_STANCE, None
            else:
                if standing_move_potential > kneeling_move_potential:
                    return ActionType.RAISE_STANCE, None

        if not knowledge.can(trooper, ActionType.MOVE):
            return

        context.step += 1
        context.is_stuck = False
        return ActionType.MOVE, pos

    def heal_weakest_neighboor(self, trooper, knowledge, hp_threshold=90, medikit_hp_threshold=50):
        neighboors = knowledge.get_neighboor_teammates(trooper)
        neighboors.append(trooper)
        weakest_hitpoints, weakest = min((mate.hitpoints, mate) for mate in neighboors)
        if hp_threshold is None or weakest_hitpoints < hp_threshold:
            if weakest_hitpoints < medikit_hp_threshold and trooper.holding_medikit:
                if knowledge.can_use_medikit(trooper):
                    return ActionType.USE_MEDIKIT, (weakest.x, weakest.y)

            if knowledge.can_heal(trooper) and weakest.hitpoints != weakest.maximal_hitpoints:
                return ActionType.HEAL, (weakest.x, weakest.y)

    def select_new_target(self, trooper, knowledge, context):

        last_noticed_enemy_pos = knowledge.enemies_tracking.get_last_noticed_enemy_position()
        if last_noticed_enemy_pos:
            knowledge.enemies_tracking.forget_last_noticed_enemy()
            print "Navigating to last seen enemy", last_noticed_enemy_pos
            return last_noticed_enemy_pos, None

        if knowledge.disposition_info:
            target = knowledge.use_disposition_info_as_target()
            print "Navigating to weakest enemy", target
            return knowledge.world_map.find_free_cell_nearby(*target), None

        nearest_bonus, nearest_path = knowledge.get_nearest_available_bonus(trooper)

        if nearest_bonus is not None:
            print "Navigating to bonus", nearest_bonus
            return (nearest_bonus.x, nearest_bonus.y), nearest_path

        target = knowledge.world_map.get_random_free_cell()
        print "Navigating to random cell", target
        return target, None

    def get_first_move(self, trooper, knowledge, context):
        return self.get_move(trooper, knowledge, context)

    def get_move(self, trooper, knowledge, context):
        raise NotImplementedError

    def commit_suicide_if_win(self, trooper, knowledge, context):
        # NOTE: NEED TO TEST
        return None

        if knowledge.makes_sense_to_commit_suicide():
            if knowledge.can_throw_grenade(trooper):
                can_commit_suicide, position = knowledge.can_commit_suicide(trooper)
                if can_commit_suicide:
                    print "PLAYER DECIDED TO DIE"
                    return ActionType.THROW_GRENADE, position


def get_random_direction():
    if getrandbits(1):
        return Direction.NORTH if getrandbits(1) else Direction.SOUTH
    else:
        return Direction.WEST if getrandbits(1) else Direction.EAST


class TargetMovingShooter(BehaviorBase):

    #move_target = knowledge.world_map.find_free_cell_nearby(*move_target)

    def get_first_move(self, trooper, knowledge, context):
        action = self.setup_target_path(trooper, knowledge, context)
        if action is not None:
            return action
        return self.get_move(trooper, knowledge, context)

    def setup_target_path(self, trooper, knowledge, context):
        if context.path is None and context.move_target is not None:
            context.step = 0
            context.path = knowledge.world_map.get_route(
                (trooper.x, trooper.y), context.move_target)

    def get_move(self, trooper, knowledge, context):

        action = self.commit_suicide_if_win(trooper, knowledge, context)
        if action is not None:
            return action

        action = self.heal_weakest_neighboor(trooper, knowledge)
        if action is not None:
            return action

        if (knowledge.can_request_enemy_disposition(trooper) and
                knowledge.spent_at_least_n_moves_without_enemy(MOVES_BEFORE_ASKING_FOR_DISPOSITION_INFO) and
                knowledge.spent_at_least_n_moves_without_damage(MOVES_BEFORE_ASKING_FOR_DISPOSITION_INFO - 2) and
                not knowledge.disposition_info):

            print "Request fresh enemy disposition info because didn't request for %d moves" % MOVES_BEFORE_ASKING_FOR_DISPOSITION_INFO

            return ActionType.REQUEST_ENEMY_DISPOSITION, None

        if trooper.stance == TrooperStance.STANDING and \
                knowledge.world.move_index >= context.max_moves_without_kneeling and \
                trooper.action_points <= knowledge.constants.stance_change_cost + 1 and \
                knowledge.can_lower_stance(trooper):
            return ActionType.LOWER_STANCE, None

        # Choosing new target
        if not context.has_path_to_walk:
            self.setup_target_path(trooper, knowledge, context)

        moving = self.move_in_path_if_make_sense(trooper, knowledge, context)

        if moving is not None:
            return moving

        if trooper.action_points == trooper.initial_action_points:
            context.is_stuck = True
            if knowledge.can_request_enemy_disposition(trooper):
                return ActionType.REQUEST_ENEMY_DISPOSITION, None

        if knowledge.leader_is_stuck() and not context.is_leader:
            action = self.move_in_random_direction(trooper, knowledge)
            if action is not None:
                return action


class WeakFollowingHealer(TargetMovingShooter):

    def setup_target_path(self, trooper, knowledge, context):
        mate = self.get_weak_teammate(trooper, knowledge)
        if mate is None:
            mate = self.get_leader_to_follow(trooper, knowledge, context)

        forbidden_positions = knowledge.get_teammates_positions(trooper)

        if mate is not None:
            target = mate.x, mate.y
            context.set_move_target(
                target,
                path=knowledge.world_map.get_route(
                        (trooper.x, trooper.y),
                        (mate.x, mate.y),
                        forbidden_positions=forbidden_positions))
        elif context.move_target is not None and context.path is None:
            context.path = knowledge.world_map.get_route((trooper.x, trooper.y), context.move_target, forbidden_positions=forbidden_positions)
            context.step = 0
        else:
            new_move_target = self.select_new_target(trooper, knowledge, context)
            context.set_move_target(
                new_move_target,
                path=knowledge.world_map.get_route((trooper.x, trooper.y), new_move_target, forbidden_positions=forbidden_positions))

        super(WeakFollowingHealer, self).setup_target_path(trooper, knowledge, context)

    def get_move(self, trooper, knowledge, context):

        action = self.commit_suicide_if_win(trooper, knowledge, context)
        if action is not None:
            return action

        action = self.heal_weakest_neighboor(trooper, knowledge, hp_threshold=90)
        if action is not None:
            return action

        if not context.has_path_to_walk:
            self.setup_target_path(trooper, knowledge, context)

        action = self.move_in_path_if_make_sense(trooper, knowledge, context, stop_nearby=True)
        if action is not None:
            #print "DECIDED TO MOVE", action
            return action

        if knowledge.leader_is_stuck():
            action = self.move_in_random_direction(trooper, knowledge)
            if action is not None:
                return action

        if trooper.stance == TrooperStance.STANDING and knowledge.can_lower_stance(trooper) and knowledge.world.move_index >= context.max_moves_without_kneeling:
            return ActionType.LOWER_STANCE, None

        action = self.heal_weakest_neighboor(trooper, knowledge, hp_threshold=None)
        if action is not None:
            return action

        if trooper.action_points == trooper.initial_action_points:
            context.is_stuck = True

        if trooper.stance == TrooperStance.KNEELING and knowledge.can_lower_stance(trooper) and knowledge.world.move_index >= context.max_moves_without_kneeling:
            return ActionType.LOWER_STANCE, None

    def get_weak_teammate(self, trooper, knowledge, hp_threshold=90):
        teammates = knowledge.get_teammates(trooper)
        if teammates:
            weakest_hitpoints, weakest = min((m.hitpoints, m) for m in teammates)
            if weakest_hitpoints < hp_threshold:
                print "GOT WEAK MEMBER TO FOLLOW:", weakest.type
                return weakest


class LeaderOrFollowingShooter(TargetMovingShooter):

    def setup_target_path(self, trooper, knowledge, context):
        forbidden_positions = knowledge.get_teammates_positions(trooper)

        if context.is_leader:
            if knowledge.enemies_tracking.last_noticed_enemy:
                context.move_target = None

            if knowledge.disposition_info:
                context.move_target = None

            if (context.move_target is None
                or (trooper.x, trooper.y) == context.move_target
                or not context.has_path_to_walk):

                    new_move_target, path = self.select_new_target(trooper, knowledge, context)
                    if path is None:
                        knowledge.world_map.get_route(
                            (trooper.x, trooper.y),
                            new_move_target, forbidden_positions=forbidden_positions)

                    context.set_move_target(
                        new_move_target,
                        path=path)

            if context.has_path_to_walk:
                next_pos = context.get_next_step()
                if knowledge.has_trooper_at(next_pos):
                    context.step = 0
                    # try to get a new route unblocking ourselves from followers
                    context.path = knowledge.world_map.get_route(
                        (trooper.x, trooper.y), context.move_target,
                        forbidden_positions=forbidden_positions)

                    if context.path is None:
                        print "LEADER IS STUCK"
                        context.is_stuck = True
        else:
            mate = self.get_leader_to_follow(trooper, knowledge, context)
            if mate is None:
                target, path = self.select_new_target(trooper, knowledge, context)
                if path is None:
                    path = knowledge.world_map.get_route((trooper.x, trooper.y), target,
                        forbidden_positions=forbidden_positions)
            else:
                target = mate.x, mate.y
                leader_context = knowledge.get_trooper_context(mate)
                if leader_context.path:
                    extended_forbidden_positions = forbidden_positions + leader_context.path[leader_context.step:]
                else:
                    extended_forbidden_positions = forbidden_positions
                path = knowledge.world_map.get_route((trooper.x, trooper.y), target,
                    forbidden_positions=extended_forbidden_positions)

            context.set_move_target(
                target,
                path=path)

        super(LeaderOrFollowingShooter, self).setup_target_path(trooper, knowledge, context)


class OptimalAttackTactic(BehaviorBase):

    def __init__(self, goal_function=attack_goal_function, backup_behavior=LeaderOrFollowingShooter(), timeout=1.3, verbose=True, debug=False):
        self.goal_function = goal_function
        self.timeout = timeout
        # TODO:
        # Backup behavior should be dynamic.
        # After a hot fight ends we should heal ourselves first
        # And then go look for enemies
        self.backup_behavior = backup_behavior
        super(OptimalAttackTactic, self).__init__(verbose=verbose, debug=debug)

    def get_first_move(self, trooper, knowledge, context):
        enemies = knowledge.get_enemies()
        context.enemies_known_at_start = enemies
        context.enemy_ids_known_at_start = set(e.id for e in context.enemies_known_at_start)
        context.bonuses_known_at_start = knowledge.get_bonuses()

        # If we see enemy at the scene - find optimal tactic and start execution
        if enemies:
            knowledge.last_move_with_enemies = knowledge.world.move_index
            self.set_optimal_tactic_in_context(trooper, knowledge, context)
            return self.get_move(trooper, knowledge, context, is_first=True)
        else:
            return self.backup_behavior.get_first_move(trooper, knowledge, context)

    def set_optimal_tactic_in_context(self, trooper, knowledge, context):
        actions = self.find_optimal_tactic(trooper, knowledge, context)
        if actions is not None:
            context.set_precalculated_actions(actions)
            context.is_stuck = False
            return True
        else:
            #print "CANNOT FIND A GOOD TACTIC", context._precalculated_actions, context.action_index
            context.is_stuck = True
            return False

    def get_move(self, trooper, knowledge, context, is_first=False):

        if not is_first:
            enemies = knowledge.get_enemies()
            enemy_ids = set(e.id for e in enemies)

            # Discovered new enemy at the middle of our tactic execution.
            # We will rerun tactic selection to adapt to changed circumstances
            if not context.enemy_ids_known_at_start.issuperset(enemy_ids):
                print "Realigning because of the new enemy at the scene"
                context.enemy_ids_known_at_start = enemy_ids
                self.set_optimal_tactic_in_context(trooper, knowledge, context)

            #bonuses = knowledge.get_bonuses()

        if context.has_precalculated_actions:
            action = context.get_next_precalculated_action()
            #print "Got next action from optimal strategy:", get_action_repr(action)
            if action is not None and knowledge.can(trooper, action):
                if action[0] == ActionType.MOVE:
                    context.path = None
                return action
            else:
                if self.debug and action[0] not in (ActionType.HEAL, ActionType.USE_MEDIKIT):
                    raise Exception("CANNOT PERFORM SCHEDULED ACTION! ERROR IN OPTIMIZER!")
        elif not context.is_stuck:
            # if there was a precalculated chain and we've finished it - end turn.
            if enemies:
                return (ActionType.END_TURN, None)

        return self.backup_behavior.get_move(trooper, knowledge, context)

    def find_optimal_tactic(self, trooper, knowledge, context):

        best_actions, best_score, best_state = actions_combinations.get_best_actions(
            trooper, knowledge, self.goal_function, verbose=self.verbose, timeout=self.timeout)

        if best_actions is None:
            return None

        converted_actions = [(a.type, a.position) for a in best_actions]
        if self.verbose:
            print "BEST ACTIONS FOUND, SCORE", best_score
            print '->'.join(map(get_action_repr, converted_actions))
        return converted_actions


class UltimateTactic(BehaviorBase):

    def __init__(self, verbose=False, debug=False):
        self.attack_tactic = OptimalAttackTactic(
            attack_goal_function,
            backup_behavior=self,
            verbose=verbose,
            debug=debug)

        super(UltimateTactic, self).__init__(verbose=verbose, debug=debug)

    def get_first_move(self, trooper, knowledge, context):

        context.tactic = self

        enemies = knowledge.get_enemies()

        if enemies:
            knowledge.last_move_with_enemies = knowledge.world.move_index

            actions = self.get_attack_actions(trooper, knowledge, context)
            if actions is None:
                actions = self.get_hideout_actions(trooper, knowledge, context)
                if actions is None:
                    actions = self.get_run_from_enemies_actions(trooper, knowledge, context)
                    if actions is None:
                        actions = self.get_rude_attack_actions(trooper, knowledge, context)

        else:
            bonus, path = knowledge.get_nearest_available_bonus(trooper)
            if bonus is not None:
                actions = self.get_bonus_picking_actions(bonus, bonus_path=path)
            else:
                if self.team_is_far(trooper):
                    actions = self.get_reunite_actions(trooper, knowledge, context)
                else:
                    actions = self.get_move_actions(trooper, knowledge, context)

        if actions is not None:
            context.set_precalculated_actions(actions)
            context.is_stuck = False
        else:
            context.is_stuck = True

        return self.get_move(self, trooper, knowledge, context, is_first=True)

    def get_move(self, trooper, knowledge, context, is_first=False):
        pass


DefaultBehavior = LeaderOrFollowingShooter
