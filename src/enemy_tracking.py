from collections import defaultdict
from model_types import TrooperType


class TrooperTracking(object):

    def __init__(self, trooper_type):
        self.id = None
        self.type = trooper_type
        self.killed = False
        self.player_id = None

        self.noticed_at = None
        self.last_seen_at = None
        self.last_seen_hitpoints = None
        self.last_seen_move_index = None
        self.noticed_by_id = None

    def team_had_moves_since_last_seen(self, knowledge, current_trooper):
        return not (
            knowledge.world.move_index == self.noticed_at
            and current_trooper.id == self.noticed_by_id
        )

    def set_killed(self, enemy=None):
        self.killed = True
        self.last_seen_hitpoints = 0

        if enemy is not None:
            self.last_seen_at = enemy.x, enemy.y

    def set_last_seen(self, enemy, knowledge, noticed_by):
        self.last_seen_at = enemy.x, enemy.y
        self.killed = False
        self.noticed_at = knowledge.world.move_index
        self.noticed_by_id = noticed_by.id
        self.last_seen_hitpoints = enemy.hitpoints


class EnemyTracking(object):

    def __init__(self, team_size):
        self.troopers = {}
        self.killed = False
        self.medic_killed = False
        for trooper_type in range(team_size):
            self.troopers[trooper_type] = TrooperTracking(trooper_type)

    def __getitem__(self, type):
        return self.troopers[type]

    def all_troopers_killed(self):
        return all(
            t.killed for t in self.troopers.itervalues())

    def set_killed(self):
        if self.killed:
            return

        self.killed = True
        for t in self.troopers.itervalues():
            t.set_killed()

    def set_player_id(self, id):
        self.id = id
        for t in self.troopers.itervalues():
            t.player_id = id


class EnemiesTracking(object):

    def __init__(self, players, team_size, player_id):
        self.player_id = player_id
        self.n_enemy_players = len(players) - 1
        self.team_size = team_size
        self.last_noticed_enemy = None
        self.last_seen_at_index = {}
        self.enemies = defaultdict(lambda: EnemyTracking(team_size))

        for p in players:
            if p.id == player_id:
                continue
            self.enemies[p.id].set_player_id(p.id)

    def update_knowledge(self, knowledge, trooper=None):
        self.world = knowledge.world
        min_hitpoints = 1000
        for e in knowledge.get_enemies_iter():
            player_id = e.player_id

            info = self.enemies[player_id][e.type]
            info.id = e.id
            self.set_last_seen(e, knowledge, trooper)

            if e.hitpoints < min_hitpoints:
                self.last_noticed_enemy = info
                min_hitpoints = e.hitpoints

        if knowledge.disposition_info:
            for player_id in knowledge.disposition_info.killed_players:
                self.player_killed(player_id)

    def player_killed(self, player_id):
        self.enemies[player_id].set_killed()

    def enemy_killed(self, enemy):
        #print "NOTE ENEMY DEATH", enemy.type, enemy.player_id
        self.enemies[enemy.player_id][enemy.type].killed = True
        if enemy.type == TrooperType.FIELD_MEDIC:
            self.enemies[enemy.player_id].medic_killed = True

        if self.enemies[enemy.player_id].all_troopers_killed():
            #print "NOTE ENEMY PLAYER DEATH", enemy.player_id
            self.player_killed(enemy.player_id)

    def get_last_noticed_enemy_position(self):
        if not self.last_noticed_enemy:
            return None
        else:
            return self.last_noticed_enemy.last_seen_at

    def forget_last_noticed_enemy(self):
        self.last_noticed_enemy = None

    def get_min_alive_enemies_ids(self):
        return [
            e.id
            for e in self.enemies.itervalues()
            if not e.killed
        ]

    def get_last_seen_at(self, position):
        return self.last_seen_at_index.get(position)

    def set_last_seen(self, enemy, knowledge, noticed_by):
        position = (enemy.x, enemy.y)
        trooper_tracking = self.enemies[enemy.player_id][enemy.type]
        self.last_seen_at_index[position] = trooper_tracking
        trooper_tracking.set_last_seen(enemy, knowledge, noticed_by)
