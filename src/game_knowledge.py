from __future__ import division

from collections import defaultdict, namedtuple
from model_types import TrooperStance, TrooperType, ActionType, BonusType
from trooper_context import TrooperContext
from map import Map
from enemy_tracking import EnemiesTracking


HitpointsInfo = namedtuple("HitpointsInfo", "hitpoints, diff")

def get_troopers_of_player(troopers, player_id):
    return filter(lambda t: t.player_id == player_id, troopers)


class DispositionInfo(object):

    def __init__(self, knowledge):
        assert knowledge.has_disposition_info()

        strategy_not_crashed, score, (px, py) = min(
            (not p.strategy_crashed,
                p.score,
                (p.approximate_x, p.approximate_y))
            for p in knowledge.get_enemy_players_iter()
            if p.approximate_x != -1 and p.approximate_y != -1)

        self.killed_players = [
            p.id
            for p in knowledge.get_enemy_players_iter()
            if p.approximate_x == -1
        ]

        self.weakest_enemy_position = (px, py)
        self.move_index = knowledge.world.move_index


class GameKnowledge(object):

    KILL_POINTS = 25
    KILL_TEAM_POINTS = 50
    KILL_ALL_POINTS = 100

    TROOPER_TYPES = (
        TrooperType.COMMANDER,
        TrooperType.FIELD_MEDIC,
        TrooperType.SOLDIER,
        TrooperType.SNIPER,
        TrooperType.SCOUT,
    )

    def __init__(self, player_id, world, game):
        self.disposition_info = None
        self.player_id = player_id
        self.constants = game
        self.world_map = Map(world)
        self.last_move_with_enemies = -1
        self.last_move_with_damage = -1
        self.world = world

        my_troopers = get_troopers_of_player(world.troopers, player_id)
        self.initial_team_size = len(my_troopers)

        self.troopers_turns_order = None
        self.is_troopers_order_known = False

        self.known_hitpoints_index = {}

        self.enemies_tracking = EnemiesTracking(
            world.players, self.initial_team_size, player_id=player_id)

        self.init_static_action_cost_map(game)

        self.troopers_context = dict(
            (trooper_type, TrooperContext(trooper_type))
            for trooper_type in self.TROOPER_TYPES)

    @property
    def is_game_start(self):
        return self.world.move_index == 0

    def init_static_action_cost_map(self, constants):
        self.STATIC_ACTION_COST_MAP = {
            ActionType.END_TURN: 0,
            #MOVE = 1
            #SHOOT = 2
            ActionType.RAISE_STANCE: constants.stance_change_cost,
            ActionType.LOWER_STANCE: constants.stance_change_cost,
            ActionType.THROW_GRENADE: constants.grenade_throw_cost,
            ActionType.USE_MEDIKIT: constants.medikit_use_cost,
            ActionType.EAT_FIELD_RATION: constants.field_ration_eat_cost,
            ActionType.HEAL: constants.field_medic_heal_cost,
            ActionType.REQUEST_ENEMY_DISPOSITION: constants.commander_request_enemy_disposition_cost,
        }

    def update_world(self, new_world, trooper, is_first_move=False):
        self.world = new_world
        self.world_map.update_world(new_world)

        if self.has_disposition_info():
            self._preserve_disposition_info()

        self.enemies_tracking.update_knowledge(self, trooper)

        if is_first_move:
            self._update_last_known_hitpoints_at_move_start()

    def _preserve_disposition_info(self):
        self.disposition_info = DispositionInfo(self)

    def set_next_trooper_order(self, trooper):
        if self.is_troopers_order_known:
            return

        if self.troopers_turns_order is not None and \
                len(self.troopers_turns_order) == self.initial_team_size:
            self.is_troopers_order_known = True

            for i, trooper_type in enumerate(self.troopers_turns_order):
                prev_trooper_type = self.troopers_turns_order[i - 1] \
                        if i > 0 \
                        else self.troopers_turns_order[-1]
                next_trooper_type = self.troopers_turns_order[i + 1] \
                        if i < (len(self.troopers_turns_order) - 1) \
                        else self.troopers_turns_order[0]
                self.troopers_context[trooper_type].set_turn_order(
                    prev_trooper_type,
                    next_trooper_type)
            return

        if self.troopers_turns_order is None:
            self.troopers_turns_order = []

        self.troopers_turns_order.append(trooper.type)

    def get_trooper_context(self, trooper):
        return self.troopers_context[trooper.type]

    def get_bonuses(self):
        return self.world.bonuses

    def get_bonus_at(self, pos):
        x, y = pos
        for u in self.world.bonuses:
            if u.x == x and u.y == y:
                return u

    def get_enemies(self, trooper=None):
        return list(self.get_enemies_iter())

    def get_enemies_iter(self):
        return (e for e in self.world.troopers
            if not e.teammate
            and e.hitpoints > 0)

    def get_shootable_enemies(self, trooper_or_position, stance=None, shooting_range=None):
        return [
            e for e in self.get_enemies_iter()
            if self.is_shootable(trooper_or_position, e, stance_from=stance, shooting_range=shooting_range)
        ]

    def is_shootable(self, trooper_from, trooper_to, stance_from=None, shooting_range=None, stance_to=None):
        return self.world.is_visible(
            trooper_from.shooting_range if shooting_range is None else shooting_range,
            trooper_from.x, trooper_from.y,
            trooper_from.stance if stance_from is None else stance_from,
            trooper_to.x, trooper_to.y,
            trooper_to.stance if stance_to is None else stance_to)

    def is_shootable_position(self, trooper_from, x, y, stance):
        return self.world.is_visible(
            trooper_from.shooting_range,
            trooper_from.x, trooper_from.y, trooper_from.stance,
            x, y, stance)

    def is_visible(self, trooper_from, unit_to, stance=None, vision_range=None):
        return self.world.is_visible(
            trooper_from.vision_range if vision_range is None else vision_range,
            trooper_from.x, trooper_from.y,
            trooper_from.stance,
            unit_to.x, unit_to.y,
            unit_to.stance
                if hasattr(unit_to, 'stance')
                else stance if stance is not None else TrooperStance.PRONE)

    def get_teammates(self, trooper):
        return list(self.get_teammates_iter(trooper))

    def get_teammates_iter(self, trooper):
        return (
            m for m in self.world.troopers
            if m.player_id == self.player_id
            and m.id != trooper.id
            and m.hitpoints > 0
        )

    def get_teammates_positions(self, trooper):
        return [
            (m.x, m.y) for m in self.get_teammates_iter(trooper)
        ]

    def get_neighboor_troopers_iter(self, trooper_or_position):
        return (
            m for m in self.world.troopers
            #if m.player_id == self.player_id
            #and m.id != trooper.id
            if ((m.y == trooper_or_position.y and abs(m.x - trooper_or_position.x) == 1) or
                 (m.x == trooper_or_position.x and abs(m.y - trooper_or_position.y) == 1))
        )

    def get_neighboor_teammates_positions(self, trooper_or_position):
        return [(m.x, m.y) for m in self.get_neighboor_teammates_iter(trooper_or_position)]

    def get_neighboor_teammates_iter(self, trooper_or_position):
        return (
            m for m in self.world.troopers
            if m.player_id == self.player_id
            #and m.id != trooper.id
            and ((m.y == trooper_or_position.y and abs(m.x - trooper_or_position.x) == 1) or
                 (m.x == trooper_or_position.x and abs(m.y - trooper_or_position.y) == 1))
        )

    def get_neighboor_teammates(self, trooper_or_position):
        return list(self.get_neighboor_teammates_iter(trooper_or_position))

    def has_medic_nearby(self, trooper_or_position):
        return any(t.type == TrooperType.FIELD_MEDIC for t in self.get_neighboor_teammates_iter(trooper_or_position))

    def can_shoot(self, trooper):
        return trooper.action_points >= trooper.shoot_cost

    def can_heal(self, trooper):
        if trooper.type != TrooperType.FIELD_MEDIC:
            return False
        dynamic_healing_ability = self.troopers_context[trooper.type].can_heal
        return dynamic_healing_ability and \
            trooper.action_points >= self.constants.field_medic_heal_cost

    def can_use_medikit(self, trooper):
        if not trooper.holding_medikit:
            return False
        dynamic_healing_ability = self.troopers_context[trooper.type].can_heal
        return dynamic_healing_ability and \
            trooper.action_points >= self.constants.medikit_use_cost

    def can_throw_grenade(self, trooper):
        if not trooper.holding_grenade:
            return False
        return trooper.action_points >= self.constants.grenade_throw_cost

    def can_eat_field_ration(self, trooper):
        if not trooper.holding_field_ration:
            return False
        return trooper.action_points >= self.constants.field_ration_eat_cost

    def can_request_enemy_disposition(self, trooper):
        if trooper.type != TrooperType.COMMANDER:
            return False
        return trooper.action_points >= self.constants.commander_request_enemy_disposition_cost

    def can_lower_stance(self, trooper):
        if trooper.stance == TrooperStance.PRONE:
            return False
        return trooper.action_points >= self.constants.stance_change_cost

    def can_raise_stance(self, trooper):
        if trooper.stance == TrooperStance.STANDING:
            return False
        return trooper.action_points >= self.constants.stance_change_cost

    def can_move(self, trooper):
        return trooper.action_points >= self.get_action_cost(trooper, ActionType.MOVE)

    def can(self, trooper, action):
        if isinstance(action, tuple):
            action = action[0]

        if action == ActionType.SHOOT:
            return self.can_shoot(trooper)
        elif action == ActionType.MOVE:
            return self.can_move(trooper)
        elif action == ActionType.HEAL:
            return self.can_heal(trooper)
        elif action == ActionType.USE_MEDIKIT:
            return self.can_use_medikit(trooper)
        elif action == ActionType.THROW_GRENADE:
            return self.can_throw_grenade(trooper)
        elif action == ActionType.EAT_FIELD_RATION:
            return self.can_eat_field_ration(trooper)
        elif action == ActionType.REQUEST_ENEMY_DISPOSITION:
            return self.can_request_enemy_disposition(trooper)
        elif action == ActionType.END_TURN:
            return True
        elif action == ActionType.LOWER_STANCE:
            return self.can_lower_stance(trooper)
        elif action == ActionType.RAISE_STANCE:
            return self.can_raise_stance(trooper)
        return False

    def get_points_to_change_stance(self, stancea, stanceb):
        return self.constants.stance_change_cost*abs(stancea - stanceb)

    def get_move_cost(self, stance):
        if stance == TrooperStance.STANDING:
            return self.constants.standing_move_cost
        elif stance == TrooperStance.KNEELING:
            return self.constants.kneeling_move_cost
        elif stance == TrooperStance.PRONE:
            return self.constants.prone_move_cost

    def get_action_cost(self, trooper, action):
        if action == ActionType.SHOOT:
            return trooper.shoot_cost
        elif action == ActionType.MOVE:
            return self.get_move_cost(trooper.stance)
        else:
            return self.STATIC_ACTION_COST_MAP[action]

    def get_static_action_cost(self, action):
        return self.STATIC_ACTION_COST_MAP[action]

    def has_trooper_at(self, pos):
        x, y = pos
        return any(t.x == x and t.y == y for t in self.world.troopers)

    def get_trooper_at(self, pos):
        x, y = pos
        for t in self.world.troopers:
            if t.x == x and t.y == y:
                return t

    def get_enemies_within_grenade_range(self, trooper_or_position):
        return [
            e for e in self.get_enemies_iter()
            if self.is_within_range(
                self.constants.grenade_throw_range,
                trooper_or_position, e)
        ]

    def get_enemies_who_could_shoot_at(self, unit, stance):
        return [
            e
            for e in self.get_enemies_iter()
            if self.is_shootable_position(e, unit.x, unit.y, stance)
        ]

    def get_enemies_who_could_see_at(self, unit, stance):
        return [
            e
            for e in self.get_enemies_iter()
            if self.is_visible(e, unit, stance=stance)
        ]

    def is_cell_within_range(self, max_range, viewer_x, viewer_y, object_x, object_y):
        x_range = object_x - viewer_x
        y_range = object_y - viewer_y

        return x_range * x_range + y_range * y_range <= max_range * max_range

    def is_within_range(self, max_range, viewer, unit):
        return self.is_cell_within_range(max_range, viewer.x, viewer.y, unit.x, unit.y)

    def get_my_troopers(self):
        return get_troopers_of_player(self.world.troopers, self.player_id)

    def get_missing_bonuses_iter(self, trooper):
        return (
            b for b in self.world.bonuses
            if not self.trooper_has_bonus(trooper, b.type))

    def trooper_has_bonus(self, trooper, bonus_type):
        if bonus_type == BonusType.GRENADE:
            return trooper.holding_grenade
        if bonus_type == BonusType.MEDIKIT:
            return trooper.holding_medikit
        if bonus_type == BonusType.FIELD_RATION:
            return trooper.holding_field_ration

    def has_disposition_info(self):
        return any(
            p.id != self.player_id and
            p.approximate_x != -1
            for p in self.get_enemy_players_iter())

    def leader_is_stuck(self):
        for context in self.troopers_context.itervalues():
            if context.is_leader:
                return context.is_stuck

    def get_enemy_players_iter(self):
        return (
            p
            for p in self.world.players
            if p.id != self.player_id)

    TEAM_MASSES = {
        TrooperType.COMMANDER : 3,
        TrooperType.FIELD_MEDIC : 2,
        TrooperType.SOLDIER : 2,
        TrooperType.SNIPER : 2,
        TrooperType.SCOUT : 1,
    }

    def get_team_mass_center(self):
        team = self.get_my_troopers()
        masses_and_positions = [
            (self.TEAM_MASSES[t.type], (t.x, t.y))
            for t in team]

        masses_sum = sum(mp[0] for mp in masses_and_positions)
        inv_masses_sum = 1 / masses_sum

        additives = (
            (inv_masses_sum*mass*p.x, inv_masses_sum*mass*p.y)
            for mass, p in masses_and_positions)

        mass_center_exact = reduce(
            lambda (x1, y1), (x2, y2): (x1 + x2, y1 + y2),
            additives,
            (0, 0))

        return mass_center_exact

    def get_nearest_available_bonus(self, trooper):
        forbidden_positions = self.get_teammates_positions(trooper)
        min_distance_to_bonus = 10**5
        nearest_bonus = None
        nearest_path = None

        for bonus in self.get_missing_bonuses_iter(trooper):
            distance_estimate = bonus.get_distance_to_unit(trooper)
            if distance_estimate < min_distance_to_bonus:
                path = self.world_map.get_route(
                    (trooper.x, trooper.y), (bonus.x, bonus.y),
                    forbidden_positions=forbidden_positions)
                pathlen = len(path)
                if pathlen < min_distance_to_bonus:
                    min_distance_to_bonus = pathlen
                    nearest_bonus = bonus
                    nearest_path = path

        return nearest_bonus, nearest_path

    def is_path_achievable(self, trooper, path):
        points = trooper.action_points

        to_change_stance = self.get_points_to_change_stance(trooper.stance, TrooperStance.STANDING)

        to_move = self.constants.standing_move_cost * len(path)

        return to_change_stance + to_move <= points

    def spent_at_least_n_moves_without_enemy(self, n_moves):
        moves_without_enemies = self.world.move_index - self.last_move_with_enemies
        return moves_without_enemies >= n_moves

    def spent_at_least_n_moves_without_damage(self, n_moves):
        moves_without_damage = self.world.move_index - self.last_move_with_damage
        return moves_without_damage >= n_moves

    def use_disposition_info_as_target(self):
        assert self.disposition_info
        target = self.disposition_info.weakest_enemy_position
        print "Navigating to the weakest enemy..", target
        self.disposition_info = None
        self.last_move_with_enemies = self.world.move_index
        return target

    def filter_grenade_victims(self, position, candidates):
        x, y = position
        for c in candidates:
            if x == c.x and y == c.y:
                damage = self.constants.grenade_direct_damage
            elif ((x == c.x and abs(y - c.y) == 1)
                    or (y == c.y and abs(x - c.x) == 1)):
                damage = self.constants.grenade_collateral_damage
            else:
                damage = 0

            if damage > c.hitpoints:
                yield c

    def _get_grenade_victims_count(self, position, candidates):
        victims = 0
        for c in self.filter_grenade_victims(position, candidates):
            victims += 1
        return victims

    def can_commit_suicide(self, trooper):
        teammates = self.get_teammates(trooper)
        team_size = len(teammates)

        if trooper.hitpoints < self.constants.grenade_collateral_damage:
            positions_to_test = []
            if trooper.x > 0:
                positions_to_test.append((trooper.x - 1, trooper.y))
            if trooper.x < self.world.width - 1:
                positions_to_test.append((trooper.x + 1, trooper.y))
            if trooper.y > 0:
                positions_to_test.append((trooper.x, trooper.y - 1))
            if trooper.y < self.world.height - 1:
                positions_to_test.append((trooper.x, trooper.y + 1))

            for position in positions_to_test:
                victims = self._get_grenade_victims_count(
                    position, teammates)

                if victims == team_size:
                    return True, position

        elif trooper.hitpoints < self.constants.grenade_direct_damage:
            victims = self._get_grenade_victims_count(
                (trooper.x, trooper.y), teammates)
            if victims == team_size:
                return True, (trooper.x, trooper.y)

        return False, None

    def get_my_score(self):
        for p in self.world.players:
            if p.id == self.player_id:
                return p.score

    def get_best_enemy_score(self):
        return max(p.score for p in self.get_enemy_players_iter())

    def get_enemy_score(self, player_id):
        enemy = next(p
            for p in self.get_enemy_players_iter()
            if p.id == player_id)
        return enemy.score

    def makes_sense_to_commit_suicide(self):
        alive_enemies_ids = self.enemies_tracking.get_min_alive_enemies_ids()
        if len(alive_enemies_ids) == 1:
            best_enemy_score = self.get_best_enemy_score()
            my_score = self.get_my_score()

            if my_score <= best_enemy_score:
                return False

            alive_enemy_id = alive_enemies_ids[0]
            alive_enemy_score = self.get_enemy_score(alive_enemy_id)

            if (my_score - alive_enemy_score > self.constants.last_player_elimination_score):
                return True
        return False

    def makes_sense_to_stop_healing(self):
        alive_enemies_ids = self.enemies_tracking.get_min_alive_enemies_ids()
        if len(alive_enemies_ids) == 1:
            alive_enemy_id = alive_enemies_ids[0]
            alive_enemy_score = self.get_enemy_score(alive_enemy_id)
            best_enemy_score = self.get_best_enemy_score()
            my_score = self.get_my_score()

            if my_score <= best_enemy_score:
                return False

            my_team = self.get_my_troopers()
            total_hitpoints = sum(t.hitpoints for t in my_team)
            max_possible_enemy_score_gain = total_hitpoints + self.constants.last_player_elimination_score

            #print "DISABLE HEALING? WIN?", my_score, alive_enemy_score, max_possible_enemy_score_gain

            if my_score - alive_enemy_score > max_possible_enemy_score_gain:
                return True

        return False

    def move_post_process(self, trooper, move):
        if move is None:
            return

        action_type, position = move
        if action_type == ActionType.SHOOT:
            #print "SHOOT PROCESSING", position, [(t.x, t.y) for t in self.get_enemies_iter()]
            enemy = self.get_trooper_at(position)

            enemy_hitpoints = None

            if enemy is None:
                enemy = self.enemies_tracking.get_last_seen_at(position)
                if enemy and enemy.last_seen_at == position and not enemy.team_had_moves_since_last_seen(self, trooper):
                    enemy_hitpoints = enemy.last_seen_hitpoints
            else:
                enemy_hitpoints = enemy.hitpoints

            if enemy_hitpoints is not None and enemy_hitpoints <= trooper.damage:
                self.enemies_tracking.enemy_killed(enemy)
        elif action_type == ActionType.THROW_GRENADE:
            #print "GRENADE PROCESSING", position, [(t.x, t.y) for t in self.get_enemies_iter()]
            for victim in self.filter_grenade_victims(position, self.get_enemies_iter()):
                self.enemies_tracking.enemy_killed(victim)
        elif action_type == ActionType.HEAL or action_type == ActionType.USE_MEDIKIT:
            healed_trooper, heal_effect = self.get_healed_trooper(trooper, action_type, position)
            if healed_trooper:
                self.update_last_known_hitpoints(healed_trooper, healed_trooper.hitpoints + heal_effect)

    def get_healed_trooper(self, healer, action_type, position):
        x, y = position
        heal_target = self.get_trooper_at(position)
        max_heal_effect = heal_target.maximal_hitpoints - heal_target.hitpoints
        if max_heal_effect <= 0:
            return None, None

        if action_type == ActionType.HEAL:
            if healer.x == x and healer.y == y:
                effect = self.constants.field_medic_heal_self_bonus_hitpoints
            else:
                effect = self.constants.field_medic_heal_bonus_hitpoints
        elif action_type == ActionType.USE_MEDIKIT:
            if healer.x == x and healer.y == y:
                effect = self.constants.medikit_heal_self_bonus_hitpoints
            else:
                effect = self.constants.medikit_bonus_hitpoints

        effect = min(max_heal_effect, effect)
        return heal_target, effect

    def update_last_known_hitpoints(self, trooper, new_hitpoints):
        previous_hitpoints, diff = self.known_hitpoints_index.get(trooper.id, (None, None))

        diff = (
            new_hitpoints - previous_hitpoints
            if previous_hitpoints is not None else 0)
        if diff < 0:
            self.last_move_with_damage = self.world.move_index
        self.known_hitpoints_index[trooper.id] = (new_hitpoints, diff)

    def hitpoints_reduced_since_last_turn(self):
        print self.known_hitpoints_index
        for trooper in self.get_my_troopers():
            hitpoints, diff = self.known_hitpoints_index.get(trooper.id, (None, None))
            if diff < 0:
                return True
        return False

    def _update_last_known_hitpoints_at_move_start(self):
        for trooper in self.world.troopers:
            self.update_last_known_hitpoints(trooper, trooper.hitpoints)
