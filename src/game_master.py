from model_types import TrooperType
from game_knowledge import GameKnowledge
from tactics import get_default_tactic

from behaviors import (
    WeakFollowingHealer,
    LeaderOrFollowingShooter,
    OptimalAttackTactic,
)

from goal_functions import (
    attack_goal_function,
    attack_but_tend_to_hide_goal_function,
    heal_goal_function,
)


class GameMaster(object):

    def __init__(self, player_id, world, game, verbose=False, debug=False):
        self.healing_disabled = False
        self.player_id = player_id
        self.constants = game
        self.knowledge = GameKnowledge(player_id, world, game)
        self.tactics = get_default_tactic(self.knowledge.initial_team_size)
        self.current_move_id = None
        self.current_leader_type = None

        self.verbose = verbose
        self.debug = debug

        self.leader_behavior = OptimalAttackTactic(
            backup_behavior=LeaderOrFollowingShooter(verbose=verbose, debug=debug),
            #goal_function=attack_but_tend_to_hide_goal_function,
            goal_function=attack_goal_function,
            verbose=verbose,
            debug=debug,
        )

        self.healer_behavior = WeakFollowingHealer(verbose=verbose, debug=debug)

        self.attacking_healer_behavior = OptimalAttackTactic(
            backup_behavior=self.healer_behavior,
            goal_function=heal_goal_function,
            verbose=verbose,
            debug=debug,
        )

        self.follower_behavior = OptimalAttackTactic(
            backup_behavior=LeaderOrFollowingShooter(verbose=verbose, debug=debug),
            #goal_function=attack_but_tend_to_hide_goal_function,
            goal_function=attack_goal_function,
            verbose=verbose,
            debug=debug,
        )

    def get_move(self, trooper, world):
        current_move_id = (world.move_index, trooper.type)
        if self.current_move_id != current_move_id:
            self.current_move_id = current_move_id
            is_first_move = True
        else:
            is_first_move = False

        self.knowledge.update_world(world, trooper, is_first_move=is_first_move)

        behavior = self.get_behavior(trooper, is_first_move=is_first_move)
        context = self.knowledge.get_trooper_context(trooper)

        if is_first_move:
            self.knowledge.set_next_trooper_order(trooper)
            move_getter = behavior.get_first_move
        else:
            move_getter = behavior.get_move

        move = move_getter(trooper, self.knowledge, context)

        self.knowledge.move_post_process(trooper, move)

        return move

    def get_trooper_repr(self, type):
        return self.trooper_reprs(type)

    def get_action_repr(self, type):
        return self.action_reprs(type)

    def get_behavior(self, trooper, is_first_move):
        if not self.healing_disabled and self.knowledge.makes_sense_to_stop_healing():
            self.disable_healing()

        if not is_first_move:
            return self.tactics[trooper.type]

        troopers = self.knowledge.get_my_troopers()

        # Determine current leader (leader might change if commander dies)
        leader = self.get_leader(troopers)

        if leader.type != self.current_leader_type:
            self.current_leader_type = leader.type
            self.update_leader_in_contexts(leader.type)

        if trooper.id == leader.id:
            # NOTE: Disable medic healing ability if he is the only one left
            if not self.knowledge.get_teammates(trooper):
                context = self.knowledge.get_trooper_context(trooper)
                context.can_heal = False

            self.tactics[trooper.type] = self.leader_behavior
            return self.leader_behavior
        elif trooper.type == TrooperType.FIELD_MEDIC:
            self.tactics[trooper.type] = self.attacking_healer_behavior
            #self.healer_behavior
            return self.attacking_healer_behavior

        self.tactics[trooper.type] = self.follower_behavior
        return self.follower_behavior

    leadership_order = (
        TrooperType.COMMANDER,
        TrooperType.SOLDIER,
        TrooperType.SCOUT,
        TrooperType.SNIPER,
        TrooperType.FIELD_MEDIC
    )

    def disable_healing(self):
        for context in self.knowledge.troopers_context.itervalues():
            context.can_heal = False
        print "ASSUMING WIN - DISABLING HEALING"
        self.healing_disabled = True

    def update_leader_in_contexts(self, leader_type):
        for context in self.knowledge.troopers_context.itervalues():
            context.leader_type = leader_type
            context.is_leader = context.type == leader_type

    def get_leader(self, troopers):
        troopers_dict = dict((t.type, t) for t in troopers)
        for trooper_type in self.leadership_order:
            trooper = troopers_dict.get(trooper_type, None)
            if trooper is not None:
                return trooper

#def get_leader_and_followers_tactic(
