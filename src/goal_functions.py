from model_types import TrooperType


class GoalFunctionFeatures(object):

    near_medic = 0
    shootable_by = 0
    observable_by = 0

    def __init__(self, state, knowledge):
        self.state = state
        self.knowledge = knowledge

    @classmethod
    def calculate(cls, state, knowledge):
        self = cls(state, knowledge)

        self.near_medic = int(state.type != TrooperType.FIELD_MEDIC and knowledge.has_medic_nearby(state.position))

        self.shootable_by = len(knowledge.get_enemies_who_could_shoot_at(state.position, state.stance))

        self.observable_by = len(knowledge.get_enemies_who_could_see_at(state.position, state.stance))

        #print "OBSERVABLE BY", state.position.x, state.position.y, state.stance, self.observable_by
        self.self_healed_hitpoints = state.hitpoints - state.initial_hitpoints
        self.total_healed_hitpoints = state.healed_hitpoints
        self.mates_healed_hitpoints = state.healed_hitpoints - self.self_healed_hitpoints
        self.estimated_hitpoints_to_live = self.shootable_by*60
        if self.observable_by > self.shootable_by:
            self.estimated_hitpoints_to_live += (self.observable_by - self.shootable_by)*20

        return self

    def get_feature_vector(self):
        return [
            self.near_medic,
            self.shootable_by,
            self.observable_by,
        ]

    def get_teammates_to_heal_nearby(self):
        total_heal_effect = 0
        teammates_nearby = 0
        for teammate in \
                self.knowledge.get_neighboor_teammates_iter(
                    self.state.position):
            if teammate.id == self.state.id:
                continue
            max_heal_effect = min(
                teammate.maximal_hitpoints - teammate.hitpoints,
                0)
            total_heal_effect += max_heal_effect
            teammates_nearby += 1
        return total_heal_effect, teammates_nearby


def attack_goal_function(state, knowledge):
    features = GoalFunctionFeatures.calculate(state, knowledge)

    max_heal_effect = state.maximal_hitpoints - state.hitpoints

    near_medic_bonus = (
        max_heal_effect
        if max_heal_effect >= 25
        else 10 if max_heal_effect >= 5
        else 0)

    observable_by_bonus = (
        max_heal_effect
        if max_heal_effect >= 50
        else 25 if max_heal_effect >= 20
        else 15)

    if features.self_healed_hitpoints \
        and features.shootable_by > 0 \
        and state.hitpoints < features.estimated_hitpoints_to_live:

        non_effective_self_healing = 1
    else:
        non_effective_self_healing = 0

    # WARNING: these coefficients are COMPLETELY RANDOM.
    return (
        (
            1.1*state.gained_points
            + 25*state.killed
            + near_medic_bonus*features.near_medic
            #+ 10*int(features.observable_by == 0)
            - observable_by_bonus*features.observable_by
            + features.total_healed_hitpoints
            - 2*features.self_healed_hitpoints*non_effective_self_healing
            # Prefer to pick bonus if everything else is same
            + int(state.holding_medikit)
            + int(state.holding_grenade)
            + int(state.holding_field_ration)
            # Prefer to end in lower stance if everything else is same
            #- 0.1*state.stance
        ),
        # NOTE: these elements are just for debug info
        (features.near_medic, 'near_medic'),
        (state.killed, 'killed'),
        (state.gained_points, 'points'),
        (-features.shootable_by, 'shootable_by'),
        (-features.observable_by, 'observable_by'),
        (features.total_healed_hitpoints, 'total_healed_hitpoints'),
        (-non_effective_self_healing, 'non_effective_self_healing'),
        (features.estimated_hitpoints_to_live, 'estimated_hitpoints_to_live'),
    )

def heal_goal_function(state, knowledge):
    features = GoalFunctionFeatures.calculate(state, knowledge)

    teammates_heal_potential, teammates_nearby = features.get_teammates_to_heal_nearby()


    # WARNING: these coefficients are COMPLETELY RANDOM.
    return (
        -features.observable_by,
        (
            state.gained_points # 0..150
            + 20*state.killed
            + 10*teammates_nearby
            + teammates_heal_potential
            + 2*features.total_healed_hitpoints # 50 max
        ),
        (features.near_medic, 'near_medic'),
        (state.killed, 'killed'),
        (state.gained_points, 'points'),
        (-features.shootable_by, 'shootable_by'),
        (-features.observable_by, 'observable_by'),
        (features.total_healed_hitpoints, 'total_healed_hitpoints'),
        (teammates_heal_potential, 'teammates_heal_potential'),
        (teammates_nearby, 'teammates_nearby'),
    )

def attack_but_tend_to_hide_goal_function(state, knowledge):
    features = GoalFunctionFeatures.calculate(state, knowledge)
    # WARNING: these coefficients are COMPLETELY RANDOM.
    return (
        -features.shootable_by,
        (
            state.gained_points
            + 40*state.killed
            + 20*features.near_medic
            + features.total_healed_hitpoints
        )
    )
