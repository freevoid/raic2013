import collections
import random

from model_types import CellType, Direction
from shortest_paths import get_graph_from_map, floyd_warshall, astar_path

class Map(object):

    def __init__(self, world):
        self.width = world.width
        self.height = world.height
        self.world = world
        self.graph = get_graph_from_map(self, allow_negative_weight=False)

    def update_world(self, new_world):
        if self.world is new_world:
            return

        self.world = new_world

    def get_moveable_directions(self, position):
        x, y = position
        if self.get_cell_type(x + 1, y) == CellType.FREE:
            yield (Direction.EAST, (x + 1, y))
        if self.get_cell_type(x - 1, y) == CellType.FREE:
            yield (Direction.WEST, (x - 1, y))
        if self.get_cell_type(x, y + 1) == CellType.FREE:
            yield (Direction.SOUTH, (x, y + 1))
        if self.get_cell_type(x, y - 1) == CellType.FREE:
            yield (Direction.NORTH, (x, y - 1))

    def get_cell_type(self, x, y):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            return CellType.HIGH_COVER

        return self.world.cells[x][y]

    def get_route_astar(self, pos_from, pos_to, forbidden_positions=None):
        self.graph.clear_weight_overrides()
        if forbidden_positions is not None:
            for pos in forbidden_positions:
                self.graph.add_weight_override(pos, 1000)

        if self.get_cell_type(*pos_to) != CellType.FREE:
            return None

        if pos_from == pos_to:
            return None

        route = astar_path(self.graph, pos_from, pos_to)

        if forbidden_positions is not None:
            self.graph.clear_weight_overrides()

        if route is not None:
            return route[1:]

    def get_route_bfs(self, pos_from, pos_to):
        #print "Get route", pos_from, pos_to
        if self.get_cell_type(*pos_to) != CellType.FREE:
            return None

        if pos_from == pos_to:
            return None

        visited = set([pos_from])
        queue = collections.deque()
        queue.append(((), pos_from))

        x_from, y_from = pos_from
        x_to, y_to = pos_to

        while queue:
            #print "Queue size:", len(queue), "Visited len:", len(visited)
            path, pos = queue.popleft()
            for direction, new_pos in self.get_moveable_directions(pos):
                if new_pos in visited:
                    continue

                if new_pos == pos_to:
                    return path + (new_pos,)

                queue.append((path + (new_pos,), new_pos))
            visited.add(pos)

    get_route = get_route_astar
    #get_route = get_route_bfs

    def find_free_cell_nearby(self, cx, cy):
        if self.get_cell_type(cx, cy) == CellType.FREE:
            return cx, cy

        radius = 1
        while radius < self.world.height:
            for x in range(cx - radius, cx + radius + 1):
                if self.get_cell_type(x, cy - radius) == CellType.FREE:
                    return x, cy - radius
                if self.get_cell_type(x, cy + radius) == CellType.FREE:
                    return x, cy + radius

            for y in range(cy - radius + 1, cy + radius):
                if self.get_cell_type(cx - radius, y) == CellType.FREE:
                    return cx - radius, y
                if self.get_cell_type(cx + radius, y) == CellType.FREE:
                    return cx + radius, y

            radius += 1

    def get_random_free_cell(self):
        x = random.randrange(0, self.width)
        y = random.randrange(0, self.height)
        available = self.find_free_cell_nearby(x, y)
        return available


class MapCell(object):

    cell_type = None

    def __init__(self, cell_type):
        self.cell_type = cell_type
