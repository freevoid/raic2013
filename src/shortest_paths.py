'''
Based on BSD-licensed NetworkX library
https://github.com/networkx/networkx/blob/master/networkx/algorithms/shortest_paths/
'''
from copy import deepcopy
import math
from heapq import heappush, heappop

from model_types import CellType

__all__ = [
    'get_graph_from_map',
    'astar_path',
    'floyd_warshall',
]


class Graph(object):

    def __init__(self, data=None, **attr):
        self.graph = {}   # dictionary for graph attributes
        self.node = {}    # empty node dict (created before convert)
        self.adj = {}     # empty adjacency dict
        # attempt to load graph with data
        self.edge = self.adj

    def __iter__(self):
        return iter(self.node)

    def __contains__(self,n):
        try:
            return n in self.node
        except TypeError:
            return False

    def __len__(self):
        return len(self.node)

    def __getitem__(self, n):
        return self.adj[n]

    def add_node(self, n, attr_dict=None, **attr):
        # set up attribute dict
        if attr_dict is None:
            attr_dict=attr
        else:
            try:
                attr_dict.update(attr)
            except AttributeError:
                raise
        if n not in self.node:
            self.adj[n] = {}
            self.node[n] = attr_dict
        else: # update attr even if node already exists
            self.node[n].update(attr_dict)

    def nodes_iter(self, data=False):
        if data:
            return iter(self.node.items())
        return iter(self.node)

    def nodes(self, data=False):
        return list(self.nodes_iter(data=data))

    def add_edge(self, u, v, attr_dict=None, **attr):
        # set up attribute dictionary
        if attr_dict is None:
            attr_dict=attr
        else:
            try:
                attr_dict.update(attr)
            except AttributeError:
                raise Exception(\
                    "The attr_dict argument must be a dictionary.")
        # add nodes
        if u not in self.node:
            self.adj[u] = {}
            self.node[u] = {}
        if v not in self.node:
            self.adj[v] = {}
            self.node[v] = {}
        # add the edge
        datadict=self.adj[u].get(v,{})
        datadict.update(attr_dict)
        self.adj[u][v] = datadict
        self.adj[v][u] = datadict

    def edges(self, nbunch=None, data=False):
        return list(self.edges_iter(nbunch, data))

    def edges_iter(self, nbunch=None, data=False):
        seen={}     # helper dict to keep track of multiply stored edges
        if nbunch is None:
            nodes_nbrs = self.adj.items()
        else:
            nodes_nbrs=((n,self.adj[n]) for n in self.nbunch_iter(nbunch))
        if data:
            for n,nbrs in nodes_nbrs:
                for nbr,data in nbrs.items():
                    if nbr not in seen:
                        yield (n,nbr,data)
                seen[n]=1
        else:
            for n,nbrs in nodes_nbrs:
                for nbr in nbrs:
                    if nbr not in seen:
                        yield (n,nbr)
                seen[n] = 1
        del seen

    def get_edge_data(self, u, v, default=None):
        try:
            return self.adj[u][v]
        except KeyError:
            return default

    def copy(self):
        return deepcopy(self)

    def is_directed(self):
        return False

    def size(self, weight=None):
        s=sum(self.degree(weight=weight).values())/2
        if weight is None:
            return int(s)
        else:
            return float(s)


class DiGraph(Graph):

    def __init__(self):
        self.graph = {} # dictionary for graph attributes
        self.node = {} # dictionary for node attributes
        # We store two adjacency lists:
        # the  predecessors of node n are stored in the dict self.pred
        # the successors of node n are stored in the dict self.succ=self.adj
        self.adj = {}  # empty adjacency dictionary
        self.pred = {}  # predecessor
        self.weight_overrides = {}
        self.succ = self.adj  # successor

        self.edge=self.adj

    def add_node(self, n, attr_dict=None, **attr):
        # set up attribute dict
        if attr_dict is None:
            attr_dict=attr
        else:
            try:
                attr_dict.update(attr)
            except AttributeError:
                raise Exception(\
                    "The attr_dict argument must be a dictionary.")
        if n not in self.succ:
            self.succ[n] = {}
            self.pred[n] = {}
            self.node[n] = attr_dict
        else: # update attr even if node already exists
            self.node[n].update(attr_dict)

    def add_edge(self, u, v, attr_dict=None, **attr):
        # set up attribute dict
        if attr_dict is None:
            attr_dict=attr
        else:
            try:
                attr_dict.update(attr)
            except AttributeError:
                raise Exception(\
                    "The attr_dict argument must be a dictionary.")
        # add nodes
        if u not in self.succ:
            self.succ[u]={}
            self.pred[u]={}
            self.node[u] = {}
        if v not in self.succ:
            self.succ[v]={}
            self.pred[v]={}
            self.node[v] = {}
        # add the edge
        datadict=self.adj[u].get(v,{})
        datadict.update(attr_dict)
        self.succ[u][v]=datadict
        self.pred[v][u]=datadict

    def edges_iter(self, nbunch=None, data=False):
        if nbunch is None:
            nodes_nbrs=self.adj.items()
        else:
            nodes_nbrs=((n,self.adj[n]) for n in self.nbunch_iter(nbunch))
        if data:
            for n,nbrs in nodes_nbrs:
                for nbr,data in nbrs.items():
                    yield (n,nbr,data)
        else:
            for n,nbrs in nodes_nbrs:
                for nbr in nbrs:
                    yield (n,nbr)

    def is_directed(self):
        return True

    def clear_weight_overrides(self):
        self.weight_overrides.clear()

    def add_weight_override(self, (x, y), weight_override):
        self.weight_overrides[((x - 1, y), (x, y))] = weight_override
        self.weight_overrides[((x + 1, y), (x, y))] = weight_override
        self.weight_overrides[((x, y - 1), (x, y))] = weight_override
        self.weight_overrides[((x, y + 1), (x, y))] = weight_override

    def get_weight(self, n1, n2):
        override = self.weight_overrides.get((n1, n2))
        if override:
            return override
        return self.get_edge_data(n1, n2).get('weight', 1)


def get_graph_from_map(world_map, allow_negative_weight=False):
    g = DiGraph()
    for x in range(world_map.width):
        for y in range(world_map.height):
            node = (x, y)

            if world_map.get_cell_type(x, y) != CellType.FREE:
                continue

            g.add_node(node)
            for u, v, weight in get_edges_from_map(x, y, world_map, allow_negative_weight=False):
                g.add_edge(u, v, weight=weight)
    return g


def get_edges_from_map(x, y, world_map, allow_negative_weight=False):
    get_edge_from_map = get_static_weight
    if x > 0 and world_map.get_cell_type(x - 1, y) == CellType.FREE:
        yield get_edge_from_map(x, y, x - 1, y, world_map)
    if x < world_map.width and world_map.get_cell_type(x + 1, y) == CellType.FREE:
        yield get_edge_from_map(x, y, x + 1, y, world_map)
    if y > 0 and world_map.get_cell_type(x, y - 1) == CellType.FREE:
        yield get_edge_from_map(x, y, x, y - 1, world_map)
    if y < world_map.height and world_map.get_cell_type(x, y + 1) == CellType.FREE:
        yield get_edge_from_map(x, y, x, y + 1, world_map)



def get_static_weight(x1, y1, x2, y2, world_map):
    return (x1, y1), (x2, y2), 1.0

def get_hideouts_weight(x1, y1, x2, y2, world_map):
    current_hideouts = get_nearby_hideouts(x1, y1, world_map)
    destination_hideouts = get_nearby_hideouts(x2, y2, world_map)
    source_hideouts_rank = sum(current_hideouts)
    destination_hideouts_rank = sum(destination_hideouts)
    rank_diff = source_hideouts_rank - destination_hideouts_rank
    # Max rank diff is (3 + 3 + 3) - (0 + 0 + 0) = 9, when one node don't
    # have any hideouts and another one has HIGH at each side.
    # Making weight to be 1.0..3.0.
    # Weight is 2.0 if we are stepping into same condition
    # Weight is 1.0 if we are stepping into highly defendent area from uncovered
    # Weight is 3.0 if we are stepping into open area from highly defendent
    weight = 2.0 + rank_diff / 9.0

    return (x1, y1), (x2, y2), weight


def get_nearby_hideouts(x, y, world_map):
    if x > 0:
        yield world_map.get_cell_type(x - 1, y)
    if x < world_map.width:
        yield world_map.get_cell_type(x + 1, y)
    if y > 0:
        yield world_map.get_cell_type(x, y - 1)
    if y < world_map.height:
        yield world_map.get_cell_type(x, y + 1)


def floyd_warshall(G, weight='weight'):
    from collections import defaultdict
    # dictionary-of-dictionaries representation for dist and pred
    # use some defaultdict magick here
    # for dist the default is the floating point inf value
    dist = defaultdict(lambda : defaultdict(lambda: float('inf')))
    for u in G:
        dist[u][u] = 0
    pred = defaultdict(dict)
    # initialize path distance dictionary to be the adjacency matrix
    # also set the distance to self to 0 (zero diagonal)
    undirected = not G.is_directed()
    for u,v,d in G.edges(data=True):
        e_weight = d.get(weight, 1.0)
        dist[u][v] = min(e_weight, dist[u][v])
        pred[u][v] = u
        if undirected:
            dist[v][u] = min(e_weight, dist[v][u])
            pred[v][u] = v
    for w in G:
        for u in G:
            for v in G:
                if dist[u][v] > dist[u][w] + dist[w][v]:
                    dist[u][v] = dist[u][w] + dist[w][v]
                    pred[u][v] = pred[w][v]
    return dict(dist)


def dist_heuristic(a, b):
    (x1, y1) = a
    (x2, y2) = b
    return math.hypot(x1 - x2, y1 - y2)


def astar_path(G, source, target, heuristic=dist_heuristic, weight='weight'):
    if heuristic is None:
        # The default heuristic is h=0 - same as Dijkstra's algorithm
        def heuristic(u, v):
            return 0
    queue = [(0, hash(source), source, 0, None)]
    enqueued = {}
    explored = {}

    while queue:
        # Pop the smallest item from queue.
        _, __, curnode, dist, parent = heappop(queue)

        if curnode == target:
            path = [curnode]
            node = parent
            while node is not None:
                path.append(node)
                node = explored[node]
            path.reverse()
            return path

        if curnode in explored:
            continue

        explored[curnode] = parent

        for neighbor, w in G[curnode].items():
            if neighbor in explored:
                continue
            ncost = dist + G.get_weight(curnode, neighbor)
            if neighbor in enqueued:
                qcost, h = enqueued[neighbor]
                # if qcost < ncost, a longer path to neighbor remains
                # enqueued. Removing it would need to filter the whole
                # queue, it's better just to leave it there and ignore
                # it when we visit the node a second time.
                if qcost <= ncost:
                    continue
            else:
                h = heuristic(neighbor, target)
            enqueued[neighbor] = ncost, h
            heappush(queue, (ncost + h, hash(neighbor), neighbor,
                             ncost, curnode))

    return None
