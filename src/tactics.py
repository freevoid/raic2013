from model_types import TrooperType
from behaviors import DefaultBehavior, WeakFollowingHealer

def get_default_tactic(n):
    tactics = {
        TrooperType.COMMANDER: DefaultBehavior(),
        TrooperType.FIELD_MEDIC: WeakFollowingHealer(),
        TrooperType.SOLDIER: DefaultBehavior(),
        TrooperType.SNIPER: DefaultBehavior(),
        TrooperType.SCOUT: DefaultBehavior(),
    }
    return tactics

