from model_types import TrooperType


class TrooperContext(object):

    def __init__(self, trooper_type):
        self.type = trooper_type

        self.can_heal = trooper_type == TrooperType.FIELD_MEDIC
        self._is_stuck = False
        self.is_leader = False

        self.move_target = None
        self.path = None
        self.step = 0

        self.max_moves_without_kneeling = 2

        self.action_index = 0
        self._precalculated_actions = ()

    @property
    def get_is_stuck(self):
        return self._is_stuck

    @get_is_stuck.setter
    def is_stuck(self, value):
        if value and not self._is_stuck:
            print self.type, "IS STUCK!"
        elif not value and self._is_stuck:
            print self.type, "IS UNSTUCK!"
        self._is_stuck = value

    @property
    def has_path_to_walk(self):
        return self.path and len(self.path) != self.step

    @property
    def has_precalculated_actions(self):
        return len(self._precalculated_actions) != 0

    def get_next_precalculated_action(self):
        if not self.has_precalculated_actions:
            return

        action = self._precalculated_actions[self.action_index]
        self.action_index += 1
        if self.action_index == len(self._precalculated_actions):
            self._precalculated_actions = ()
        return action

    def set_precalculated_actions(self, actions):
        if not isinstance(actions, (list, tuple)):
            actions = list(actions)
        self._precalculated_actions = actions
        self.action_index = 0

    def set_move_target(self, target, path=None):
        self.move_target = target
        self.path = path
        self.step = 0

    def get_next_step(self):
        if self.has_path_to_walk:
            return self.path[self.step]

    def set_turn_order(self, prev_type, next_type):
        self.previous_turn_trooper_type = prev_type
        self.next_turn_trooper_type = next_type
