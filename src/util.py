from model_types import TrooperType, ActionType, Trooper


trooper_reprs = dict(
    (value, type) for type, value in vars(TrooperType).iteritems())

def get_trooper_repr(trooper):
    if isinstance(trooper, Trooper):
        type_repr = trooper_reprs[trooper.type]
        return "[%3d](%2d,%2d) %11s" % (
            trooper.hitpoints, trooper.x, trooper.y, type_repr)
    else:
        return trooper_reprs[trooper]

action_reprs = dict(
    (value, type) for type, value in vars(ActionType).iteritems())

def get_action_repr(action_type):
    if isinstance(action_type, tuple):
        action_type, position = action_type
    else:
        position = None
    type_repr = action_reprs[action_type]
    if position:
        return type_repr + '(%2d,%2d)' % position
    else:
        return type_repr
