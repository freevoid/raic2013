import sys


def parse_results_file(results_file_path):
    with open(results_file_path) as fp:
        results = [line.strip() for line in fp]

    if results[0] != 'OK':
        print "Game failed"
        print '\n'.join(results)
        return None

    seed = int(results[1].split(' ')[1])

    ranks = results[2:]
    my_status = ranks[1]

    my_rank, my_points, my_success = my_status.split(' ')
    return int(my_rank), int(my_points), my_success, seed


if __name__ == '__main__':
    results_file_path = sys.argv[1]
    my_status = my_rank, my_points, my_success, seed = parse_results_file(results_file_path)

    if my_success != 'OK':
        print "Strategy failed during local run:", my_status
        sys.exit(1)

    if my_rank != 1:
        print "Strategy was not the best during local run:", my_status
        sys.exit(1)

    sys.exit(0)
