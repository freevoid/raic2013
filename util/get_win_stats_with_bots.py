from __future__ import division
import sys
import pprint
import os
import argparse
import random
from collections import defaultdict

from check_test_run_results import parse_results_file


def read_bad_games(path):
    result = []
    with open(path) as fp:
        for line in fp:
            conf, map, seed = line.strip().split('\t')
            seed = int(seed)
            result.append((conf, map, seed))
    return result


def aggregate_local_runs(repeat_count, log_path='run.log', seeds_path=None, port=None, result_file='result.txt', map='default', game_configuration='4x3'):
    ranks_dist = defaultdict(int)
    ranks = []
    current_bad_games = []
    nfailures = 0
    pointsum = 0
    minpoints = 10**9

    if seeds_path:
        bad_games = read_bad_games(seeds_path)
        repeat_count = len(bad_games)
    else:
        bad_games = None
        seeds = [
            random.randrange(2**31)
            for i in range(repeat_count)]

    save_seeds = not seeds_path

    for i in range(repeat_count):
        if bad_games:
            game_configuration, map, seed = bad_games[i]
        else:
            seed = seeds[i]

        output_prefix = "[%7s:%3s:%2d/%2d]" % (map, game_configuration, i + 1, repeat_count)

        exit_code = os.system('make GAME_CONF=%s SEED=%s PORT=%s RESULT_FILE="%s" MAP=%s runserver run >> "%s"' % (game_configuration, seed, port, result_file, map, log_path))

        if exit_code != 0:
            print "Error while executing local runner!", game_configuration, map, seed
            nfailures += 1
            current_bad_games.append((game_configuration, map, seed))
            continue

        rank, points, success, seed = parse_results_file('result.txt')

        ranks_dist[rank] += 1
        ranks.append(rank)
        pointsum += points
        minpoints = min(minpoints, points)

        print output_prefix, "ROUND FINISHED. RANK: %d, AVERAGE RANK: %s" % (rank, sum(ranks) / (i + 1))

        if success != 'OK':
            print output_prefix, "CRASHED #%s" % nfailures
            nfailures += 1
            current_bad_games.append((game_configuration, map, seed))
        elif rank >= 3:
            current_bad_games.append((game_configuration, map, seed))

    if save_seeds and current_bad_games:
        with open("%s.seeds.txt" % log_path, "a") as fp:
            for bad_game in current_bad_games:
                data = [str(e) for e in bad_game]
                data = '\t'.join(data)
                fp.write(data)
                fp.write('\n')

    return {
        'ranks': dict(ranks_dist),
        'average_rank': sum(ranks) / repeat_count,
        'best_rank_ratio': ranks_dist.get(1, 0) / repeat_count,
        'min_rank': min(ranks),
        'max_rank': max(ranks),
        'nfailures': nfailures,
        'average_points': pointsum / repeat_count,
        'min_points': minpoints
    }


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', dest='repeat_count', default=10, type=int)
    parser.add_argument('-p', dest='preserve_run_log', action='store_true')
    parser.add_argument('-l', dest='log_path', default='run.log')
    parser.add_argument('-s', dest='seeds_path', default='')
    parser.add_argument('--conf', dest='game_configuration', default='4x3')
    parser.add_argument('--result_file', default='./result.txt')
    parser.add_argument('--map', default='default')
    parser.add_argument('--port', default=32001, type=int)
    args = parser.parse_args()

    if not args.preserve_run_log and os.path.isfile(args.log_path):
        os.remove(args.log_path)

    results = aggregate_local_runs(args.repeat_count, log_path=args.log_path, seeds_path=args.seeds_path, port=args.port, map=args.map, game_configuration=args.game_configuration)
    if results is None:
        sys.exit(1)

    pprint.pprint(results)
    if not args.seeds_path and args.repeat_count >= 10:
        with open("results.tsv", "a") as fp:
            data = [
                args.map,
                args.game_configuration,
                args.repeat_count,
                results['best_rank_ratio'],
                results['average_rank'],
                results['average_points'],
                results['min_points'],
                results['nfailures'],
            ]
            data_str = [str(d) for d in data]
            fp.write('\t'.join(data_str))
            fp.write('\n')

    has_errors = False

    if results['nfailures'] > 0:
        print "ERROR: Has crashes"
        has_errors = True
    if results['average_rank'] >= 2:
        print "ERROR: Has bad average rank"
        has_errors = True
    #if results['max_rank'] == 3:
    #    print "ERROR: Has bad max rank"
    #    has_errors = True
    if results['best_rank_ratio'] < 0.8:
        print "ERROR: Best rank ratio is low"
        has_errors = True
    if results['average_points'] < 600:
        print "ERROR: Has bad average_points"
        has_errors = True

    sys.exit(1 if has_errors else 0)
