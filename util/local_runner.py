import os
import sys
from tempfile import mkstemp
import argparse

current_folder = os.path.dirname(os.path.abspath(__file__))

LOCAL_RUNNER_COMMAND_TEMPLATE = \
    'java%(gui_suffix)s -cp ".:*:%(local_runner_folder)s/*" -jar "%(local_runner_folder)s/local-runner.jar" "%(properties_path)s" %(seed)s'

CYGWIN_PATH = r'C:\cygwin'

def get_crossplatform_path(path):
    if sys.platform == 'cygwin':
        if path.startswith('/cygdrive/'):
            drive = path[10]
            prefix = drive.upper() + ':'
            path = path[11:]
        else:
            prefix = CYGWIN_PATH

        return prefix + path.replace('/', '\\')
    else:
        return path

local_runner_folder = get_crossplatform_path(os.path.join(current_folder, '..', 'local_runner'))

def extract_parameters(template_path):
    values = {}
    with open(template_path, 'r') as fp:
        for line in fp:
            if '=' in line:
                name, value = line.split('=')
                values[name] = value
    return values

def generate_properties(template_path, values):
    fd, path = mkstemp()

    default_values = extract_parameters(template_path)
    default_values.update(values)

    with os.fdopen(fd, 'w') as fp:
        for name, value in default_values.iteritems():
            fp.write('%s=%s\n' % (name, value))

    return path, default_values

def start_local_runner(on_finish, template_path, **parameters):
    seed = parameters.pop('seed', '')
    fixed_parameters = {
        k.replace('_', '-') : v
        for (k, v) in parameters.iteritems()
        if v is not None and v != ''
    }

    try:
        properties_path, full_parameters = generate_properties(template_path, fixed_parameters)

        fixed_properties_path = get_crossplatform_path(properties_path)

        cmd = LOCAL_RUNNER_COMMAND_TEMPLATE % {
            'local_runner_folder': local_runner_folder,
            'properties_path': fixed_properties_path,
            'seed': seed,
            'gui_suffix': 'w'
                if full_parameters.get('render-to-screen') == 'true'
                else ''
        }

        print "Executing '%s'" % cmd
        print "Game properties:", ';'.join('%s=%s' % (k,v) for k, v in sorted(full_parameters.items()))

        exit_code = os.system(cmd)
        if exit_code != 0:
            print "ERROR WHILE EXECUTING LOCAL RUNNER"
        on_finish(parameters=full_parameters, exit_code=exit_code)
    finally:
        os.remove(properties_path)


def noop(**kwargs):
    pass


def print_results_file(parameters, exit_code, **kwargs):
    if exit_code != 0:
        return

    results_file_path = parameters['results-file']
    os.system('cat "%s"' % results_file_path)


def parse_game_configuration(c):
    if c:
        nplayers, team_size = map(int, c.split('x'))
        return {
            'team_size': team_size,
            'player_count': nplayers,
        }
    else:
        return {
            'team_size': 3,
            'player_count': 4,
        }


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('template_path')
    parser.add_argument('-p', '--port', dest='base_adapter_port', default='')
    parser.add_argument('-m', '--map', default='')
    parser.add_argument('-s', '--seed', default='')
    parser.add_argument('--conf', dest='game_configuration', default='4x3')
    parser.add_argument('-r', '--results-file', default='')
    parser.add_argument('-g', '--gui', action='store_true')
    args = parser.parse_args()

    args_dict = vars(args)
    template_path = args_dict.pop('template_path')
    gui = args_dict.pop('gui')
    if gui:
        args_dict['render_to_screen'] = 'true'

    args_dict.update(
        parse_game_configuration(args_dict.pop('game_configuration'))
    )

    start_local_runner(noop, template_path, **args_dict)
